# Set up development server
Assumptions:
* You have [Python 3.10+](https://www.python.org/downloads/) installed
* You have [Node.js](https://nodejs.org/en/download) installed

## Backend
### Initial setup
Install required packages:

    $ cd backend
    $ python3 -m venv venv                  # Create virtual env
    $ . venv/bin/activate                   # Activate virtual env
    $ pip install --upgrade pip             # Upgrade pip
    $ pip install -r requirements.txt       # Install neceesary packages
    $ pip install -r tests/requirements.txt # Install testing packages

From root directory, create a file with path `backend/arcnotes/arcnotes/.env`:

    SECRET_KEY=jdhfsa*dgfg37|fgwey!efw
    DEBUG=True
    CSRF_TRUSTED_ORIGINS=http://localhost:3000,http://localhost:8000
    CORS_ALLOWED_ORIGINS=http://localhost:3000
    ALLOWED_HOSTS=localhost:8000
    SESSION_COOKIE_DOMAIN=localhost
    CSRF_COOKIE_DOMAIN=localhost
    SECURE_COOKIES=True
    
    # Enable if want to use Postgres
    # POSTGRES_HOST=db.rcs.uvic.ca
    # POSTGRES_USER=arcnotes_owner
    # POSTGRES_DB=arcnotes_prod
    # POSTGRES_PASSWORD=#DB_OWNER_PW#

### Create users
There are 2 ways to create users. First way is to simply run:

    $ python3 arcnotes/manage.py createusers
    Successfully created a new user with username user@example.org
    Successfully created a new user with username user1@example.org
    Successfully created a new user with username admin@example.org
    Successfully created a new user with username admin1@example.org

These users are configured on line 100 and 101 in `settings.py` where `AUTHZ_USERS` are normal users and `AUTHZ_ADMINS` are admin users with `is_staff` set to `True`.

 
The second way is to create a default superuser with username **admin@example.org**, email **admin@example.org** and password **password123**. You can change these to whatever you want:

    $ echo "from django.contrib.auth import get_user_model; User = get_user_model(); User.objects.create_superuser('admin@example.org', 'admin@example.org', 'password123')" | python arcnotes/manage.py shell

You can then use the admin dashboard at `localhost:8000/api/admin-django` to create users with usernames that you like


### Run development server
Run the development server on port **8000**:

    $ python3 arcnotes/manage.py runserver
    Watching for file changes with StatReloader
    Performing system checks...

    System check identified no issues (0 silenced).
    April 15, 2024 - 14:03:18
    Django version 5.0.3, using settings 'arcnotes.settings'
    Starting development server at http://127.0.0.1:8000/
    Quit the server with CONTROL-C.


## Frontend
Install required packages:
    
    $ cd frontend
    $ npm i

From root directory, create a file with path `frontend/.env.local`:

    NEXT_PUBLIC_API_URL=http://localhost:8000/api
    NEXT_PUBLIC_USER_OVERRIDE=admin@example.org             # You can choose a different username based on what you created in the backend step above

Run the development server:

    $ npm run dev
