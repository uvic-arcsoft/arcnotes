"use client";

import { useState, useEffect, createContext } from 'react'
import Sidebar from '../navbars/Sidebar';
import Navbar from '../navbars/Navbar';

export const UserContext = createContext();
export const SidebarContext = createContext();
export const RootWorkspacesContext = createContext();

export default function AppRootLayout({ requestedUser, children }) {
    // Store information about the currently logged in user
    const [user, setUser] = useState({});

    // Store all of the workspaces with doc lists and docs that the currently logged in user has
    const [workspaces, setWorkspaces] = useState([]);
    
    // Use this state to avoid refetching workspaces multiple time
    const [isWorkspacesLoaded, setIsWorkspacesLoaded] = useState(false);

    useEffect(() => {
        // Log user in
        const login = async () => {
            const response = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/login`, {
                headers: {
                    "X-Forwarded-User": requestedUser,
                },
                credentials: "include"
            });
            if (response.ok) {
                const data = await response.json();
                console.log(data);
                setUser({
                    email: data.user_email,
                    isAdmin: data.is_admin,
                    firstName: data.first_name,
                    lastName: data.last_name,
                    image: data.image,
                    bio: data.bio,
                })
            } else {
                console.error("Failed to authenticate");
            }
        }

        // Get all workspaces from the backend
        const getWorkspaces = async () => {
            const res = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/workspaces/get_all`, {
                method: 'GET',
                credentials: 'include',
            });
            const data = await res.json();
            setIsWorkspacesLoaded(true);

            if (res.ok) {
                console.log('Get all workspaces successfully');
                console.log(data);
                setWorkspaces(data.workspaces);
            } else {
                console.error('Failed to fetch workspaces');
                console.log(data.errors);
            }
        }

        const init = async() => {
            // Login the user if not already logged in
            if (!user.email) {
                await login();
            }
            if (!isWorkspacesLoaded) {
                await getWorkspaces();
            }
        }

        init();
    }, []);

    const [sidebarOpen, setSidebarOpen] = useState(false)

    return (
        <UserContext.Provider value={{ user, setUser }}>
            <RootWorkspacesContext.Provider value={{ workspaces, setWorkspaces }}>
                <SidebarContext.Provider value={{ sidebarOpen, setSidebarOpen }}>
                    <Sidebar />

                    <div className="lg:pl-72">
                        <Navbar />

                        <main>{children}</main>
                    </div>
                </SidebarContext.Provider>
            </RootWorkspacesContext.Provider>
        </UserContext.Provider>
    )
}




