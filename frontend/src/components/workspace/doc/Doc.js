"use client";

import { Fragment, useState, useContext, useEffect, useRef } from "react";
import { useRouter, useSearchParams } from "next/navigation";

import { Draggable } from "@hello-pangea/dnd"
import { Menu, Transition } from '@headlessui/react';
import { EllipsisHorizontalIcon } from '@heroicons/react/20/solid';
import { CheckIcon } from "@heroicons/react/24/outline";
import {CopyToClipboard} from "react-copy-to-clipboard";

import { classNames, getCookie } from "@/helper";
import useOutsideClick from "@/hooks/useOutsideClick";
import { UserContext } from "@/components/layouts/AppRootLayout";
import { WorkspaceContext, SelectedTagsContext } from "@/app/workspace/[workspaceId]/page";
import { DocsContext } from "../DocList";
import DeleteDocDialog from "./dialogs/DeleteDocDialog";
import MoveDocDialog from "./dialogs/MoveDocDialog";
import { helpTextStyle, labelStyle } from "@/styles/form";
import DocDialog from "./dialogs/DocDialog";

export const Doc = ({ doc, index }) => {
    const {workspace} = useContext(WorkspaceContext);
    const {selectedTags} = useContext(SelectedTagsContext);

    const router = useRouter();

    // Get search params
    const searchParams = useSearchParams();
    const searchedDoc = parseInt(searchParams.get('doc'));

    // Is the doc modal open
    const [isOpen, setIsOpen] = useState(searchedDoc === doc.id);

    useEffect(() => {
        setIsOpen(searchedDoc === doc.id);
    }, [searchedDoc]);

    return (
        <>
            <Draggable draggableId={`doc-${doc.id}`} index={index}>
                {/* Doc display in a list */}
                {(provided) => (
                    <div 
                    {...provided.draggableProps}
                    {...provided.dragHandleProps}
                    ref={provided.innerRef}
                    className="relative group"
                    >
                        <div 
                            className={`${selectedTags.length === 0 || doc.tags.some(tag => selectedTags.includes(tag)) ? "" : "hidden"} w-full p-2 rounded-md text-sm cursor-pointer bg-slate-200 border-2 border-slate-200 hover:border-blue-500`}
                            onClick={() => router.push(`/workspace/${workspace.id}?doc=${doc.id}`)}
                            type="button">
                            {doc.name}
                        </div>
                        <DocDropdown doc={doc}/>
                    </div>
                )}
            </Draggable>

            {/* Dialog to view and update doc */}
            <DocDialog isOpen={isOpen} setIsOpen={setIsOpen} passedDoc={doc}/>
        </>
    )
}

const DocDropdown = ({ doc }) => {
    const {user} = useContext(UserContext);
    const [isDeleteDialogOpen, setIsDeleteDialogOpen] = useState(false);
    const [isCopyDialogOpen, setIsCopyDialogOpen] = useState(false);

    return (
        <div className="absolute top-2 right-2">
            <Menu as="span" className="relative">
                <Menu.Button>
                    <EllipsisHorizontalIcon className="hidden group-hover:block menu-icon w-5 h-5 ml-auto my-auto rounded-md duration-75 bg-white hover:text-black hover:bg-gray-200"/>
                </Menu.Button>
                <Transition
                    as={Fragment}
                    enter="transition ease-out duration-100"
                    enterFrom="transform opacity-0 scale-95"
                    enterTo="transform opacity-100 scale-100"
                    leave="transition ease-in duration-75"
                    leaveFrom="transform opacity-100 scale-100"
                    leaveTo="transform opacity-0 scale-95"
                >
                    <Menu.Items className="absolute left-[100%] top-0 w-60 z-10 mt-2.5 py-2 origin-top-right rounded-sm bg-white shadow-lg ring-1 ring-gray-900/5 focus:outline-none">
                        <Menu.Item>
                            <span className='block px-3 py-1 text-sm text-center font-semibold leading-6 text-gray-600 mb-3'>{doc.name}</span>
                        </Menu.Item>
                        {/* Contributors required */}
                        {doc.contributors.includes(user.email) &&
                        <>
                            <Menu.Item onClick={() => setIsDeleteDialogOpen(true)}>
                            {({ active }) => (
                                <span
                                    className={classNames(
                                        active ? 'bg-gray-100' : '',
                                        'block px-3 py-1 text-sm leading-6 text-gray-900 font-medium cursor-pointer'
                                    )}
                                    >
                                    Delete doc
                                </span>
                            )}
                            </Menu.Item>
                            <Menu.Item onClick={() => setIsCopyDialogOpen(true)}>
                            {({ active }) => (
                                <span
                                    className={classNames(
                                        active ? 'bg-gray-100' : '',
                                        'block px-3 py-1 text-sm leading-6 text-gray-900 font-medium cursor-pointer'
                                    )}
                                    >
                                    Move doc
                                </span>
                            )}
                            </Menu.Item>
                        </>}
                    </Menu.Items>
                </Transition>
            </Menu>

            {/* Dialog for deleting a workspace */}
            <DeleteDocDialog isOpen={isDeleteDialogOpen} setIsOpen={setIsDeleteDialogOpen} doc={doc} />

            {/* Dialog for copying doc to another list or workspace */}
            <MoveDocDialog isOpen={isCopyDialogOpen} setIsOpen={setIsCopyDialogOpen} doc={doc} />
        </div>
    )
}

export const ShareLink = ({ docUrl }) => {
    const docUrlRef = useRef(null);
    const [isCopied, setIsCopied] = useState(false);

    // Hide the "Copied" text if clicking outside of the doc URL
    useOutsideClick(docUrlRef, () => setIsCopied(false));

    return (
        <div className="text-xs">
            <label className={labelStyle.neutral}>Share doc with others</label>
            <div ref={docUrlRef} className="flex flex-row gap-2 w-fit">
                <CopyToClipboard text={docUrl} onCopy={() => setIsCopied(true)}>
                    <span className="bg-slate-200 px-4 py-2 rounded-md w-fit mt-1 cursor-pointer">
                        {docUrl || "loading doc URL..."}
                    </span>
                </CopyToClipboard>
                {isCopied && <span className="my-auto text-green-700 flex flex-row gap-1">
                    <CheckIcon className="w-4 h-4"/>
                    Copied
                </span>}
            </div>
            <p className={`${helpTextStyle.neutral} text-xs`}>Only people who have access to this workspace will have access to this doc</p>
        </div>
    )
}

export const CreateDocBtn = ({ docList }) => {
    const [state, setState] = useState('view');
    const {docs, setDocs} = useContext(DocsContext);
    const createDocFormRef = useRef(null);

    // Close doc creation form when clicked outside
    useOutsideClick(createDocFormRef, () => setState('view'));

    const createDoc = async (e) => {
        e.preventDefault();
        const res = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/docs/create`, {
            method: 'POST',
            credentials: 'include',
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': getCookie('csrftoken')
            },
            body: JSON.stringify({
                name: e.target.name.value,
                doc_list: docList.id
            })
        });
        const data = await res.json();

        if (res.ok) {
            console.log('Doc created');
            console.log(data);

            // Render the new doc
            setDocs([...docs, data.doc]);

            // Clear and close form
            e.target.reset();
            setState('view');
        } else {
            console.error('Failed to create doc');
            console.log(data.errors);
        }
    }

    if (state === "edit") {
        return (
            <div className="w-full p-1 rounded-md">
                <form onSubmit={createDoc} ref={createDocFormRef}>
                    <div className="mb-3">
                        <textarea 
                            type="text"
                            name="name"
                            rows={2}
                            className="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded-md focus:ring-blue-500 focus:border-blue-500 block w-full p-2" 
                            placeholder="Enter a name for this documentation..." 
                            required />
                    </div>
                    <button 
                        type="submit" 
                        className="text-white bg-gradient-to-r from-cyan-500 to-blue-500 hover:bg-gradient-to-bl focus:ring-4 focus:outline-none focus:ring-cyan-300 font-medium rounded-lg text-xs px-3 py-2 text-center me-2">
                        <i className="fa-solid fa-plus me-2"></i>
                        Create doc
                    </button>
                    <button 
                        type="button" 
                        className="text-white bg-gradient-to-r from-red-400 via-red-500 to-red-600 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-red-300 font-medium rounded-lg text-xs px-3 py-2 text-center me-2"
                        onClick={() => setState("view")}>
                        Cancel
                    </button>
                </form>
            </div>
        )
    }

    return (
        <div 
            className="w-full p-2 rounded-md hover:bg-slate-200 text-sm cursor-pointer"
            onClick={() => setState('edit')}>
            <i className="fa-solid fa-plus me-2"></i>
            Add a documetation
        </div>
    )
}