import "github-markdown-css/github-markdown-light.css";

import Markdown from "react-markdown"
import remarkGfm from "remark-gfm";

export default function Version({ version, updateVersion, state="edit", show=false }) {
    if (!show) return null;
    
    return (
        <div>
            {state === "edit" ? 
            <textarea 
                rows="20" 
                className="block p-4 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500" 
                placeholder="Version content. Markdown supported"
                name="content"
                onChange={updateVersion}
                value={version.content}></textarea> : 
            <div className="p-4 bg-gray-50 rounded-lg border border-gray-300 markdown-body max-w-none">
                <Markdown remarkPlugins={[remarkGfm]}>{version.content}</Markdown>
            </div>}
        </div>
    )
}
