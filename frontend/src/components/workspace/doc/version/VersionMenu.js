import { Fragment, useContext } from "react"

import { Menu, Transition } from "@headlessui/react"
import { EllipsisHorizontalIcon } from "@heroicons/react/24/outline"

import { classNames } from "@/helper"
import { UserContext } from "@/components/layouts/AppRootLayout"

export default function VersionMenu({ doc, version, deleteVersion }) {
    const {user} = useContext(UserContext);

    return (
        <Menu as="span" className="relative my-auto">
            <Menu.Button>
                <EllipsisHorizontalIcon className="menu-icon w-5 h-5 ml-auto my-auto rounded-md duration-75 bg-white hover:text-black hover:bg-gray-200"/>
            </Menu.Button>
            <Transition
                as={Fragment}
                enter="transition ease-out duration-100"
                enterFrom="transform opacity-0 scale-95"
                enterTo="transform opacity-100 scale-100"
                leave="transition ease-in duration-75"
                leaveFrom="transform opacity-100 scale-100"
                leaveTo="transform opacity-0 scale-95"
            >
                <Menu.Items className="absolute right-[100%] top-0 w-60 z-10 mt-2.5 py-2 origin-top-right rounded-sm bg-white shadow-lg ring-1 ring-gray-900/5 focus:outline-none">
                    <Menu.Item>
                        <span className='block px-3 py-1 text-sm text-center font-semibold leading-6 text-gray-600 mb-3'>{version.version}</span>
                    </Menu.Item>
                    {/* Contributors required */}
                    {doc.contributors.includes(user.email) &&
                    <>
                        <Menu.Item>
                        {({ active }) => (
                            <span
                                className={classNames(
                                    active ? 'bg-gray-100' : '',
                                    'block px-3 py-1 text-sm leading-6 text-gray-900 font-medium cursor-pointer'
                                )}
                                onClick={() => deleteVersion(version.id)}
                                >
                                Delete version
                            </span>
                        )}
                        </Menu.Item>
                    </>}
                </Menu.Items>
            </Transition>
        </Menu>
    )
}