import { useState, useRef } from "react"
import useOutsideClick from "@/hooks/useOutsideClick"
import { inputStyle } from "@/styles/form"

export default function VersionName({ version, updateVersion }) {
    const [state, setState] = useState("view")

    const formRef = useRef();

    // Close version name input on outside click
    useOutsideClick(formRef, () => {
        setState("view");
    });

    // Update version name
    const updateVersionName = (e) => {
        e.preventDefault();
        const newVersion = {...version, version: e.target.version.value};
        updateVersion(newVersion);
    }

    if (state === "edit") {
        return (
            <form ref={formRef} onSubmit={(e) => {updateVersionName(e); setState("view");}}>
                <input 
                    type="text" 
                    className={`${inputStyle.neutral}`}
                    placeholder="Version name" 
                    name="version"
                    defaultValue={version.version}
                    ></input>
            </form>
        )
    }
    
    return <span 
            className="hover:bg-gray-200 duration-75 cursor-pointer"
            onClick={() => setState("edit")}>
            {version.version}</span>
}