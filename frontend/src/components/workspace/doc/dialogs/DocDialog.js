import { useRouter, usePathname } from "next/navigation";
import { useState, useEffect, useContext } from "react";

import { PlusIcon, CheckIcon, XMarkIcon } from "@heroicons/react/24/outline";

import { WorkspaceContext } from "@/app/workspace/[workspaceId]/page";
import { DocsContext } from "../../DocList";

import { getCookie } from "@/helper";
import { buttonStyle } from "@/styles/buttons";
import CustomDialog from "@/components/CustomDialog";
import { ShareLink } from "../Doc";
import Version from "../version/Version";
import VersionName from "../version/VersionName";
import VersionMenu from "../version/VersionMenu";

export default function DocDialog({ isOpen, setIsOpen, passedDoc }) {
    const router = useRouter();
    const pathname = usePathname();

    const {workspace, setWorkspace} = useContext(WorkspaceContext);
    const {docs, setDocs} = useContext(DocsContext);
    const [doc, setDoc] = useState(passedDoc);

    // Track the latest saved doc snapshot
    const [savedDocSnapshot, setSavedDocSnapshot] = useState(passedDoc);

    // Timeout for saving doc
    let updateDocTimeout = null;

    // Are we waiting for the doc to update
    const [isUpdating, setIsUpdating] = useState(false);

    // Doc updated successfully?
    const [updatedSuccessfully, setUpdatedSuccessfully] = useState(true);

    // Doc versions
    const [versions, setVersions] = useState([]);

    // Track the latest saved versions snapshot
    const [savedVersionsSnapshot, setSavedVersionsSnapshot] = useState([]);

    // Track if each version is being edited
    const [versionStates, setVersionStates] = useState({});

    // Track which version is being opened
    const [activeVersion, setActiveVersion] = useState(null);

    // Track the main version for each doc
    const [mainVersion, setMainVersion] = useState(null);

    // Current URL of doc
    const [docUrl, setDocUrl] = useState('');

    // Track all deleted version ids
    const [deletedVersionIds, setDeletedVersionIds] = useState([]);

    // Fetch all versions of the doc
    useEffect(() => {
        const fetchVersions = async () => {
            try {
                const res = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/versions/get_all_from_doc/${doc.id}`, {
                    method: 'GET',
                    credentials: 'include',
                    headers: {
                        'Content-Type': 'application/json'
                    }
                });
                const data = await res.json();

                if (res.ok) {
                    console.log('Doc versions fetched');
                    console.log(data);

                    setVersions(data.versions);
                    setSavedVersionsSnapshot(data.versions);
                    let curVersionStates = {};
                    data.versions.forEach(v => {
                        curVersionStates[v.id] = v.content !== '' ? 'preview' : 'edit';
                    });
                    setVersionStates(curVersionStates);
                    
                    const docMainVersion = data.versions.find(v => v.is_main);
                    if (docMainVersion) {
                        setActiveVersion(docMainVersion);
                        setMainVersion(docMainVersion);
                    } else {
                        setActiveVersion(data.versions[0]);
                        setMainVersion(data.versions[0]);
                    }
                } else {
                    console.error('Failed to fetch doc versions');
                    console.log(data.message);
                }
            } catch (error) {
                console.error('Failed to fetch doc versions');
                console.log(error);
            }
        }

        if (isOpen) {
            fetchVersions();
            setIsUpdating(false);
            setUpdatedSuccessfully(true);
        }
    }, [isOpen]);

    // Display share link
    useEffect(() => {
        setDocUrl(window.location.href);
    }, [isOpen]);

    // Update versions array when active version changes
    useEffect(() => {
        if (activeVersion) {
            setVersions(prevVersions => prevVersions.map(v => v.id === activeVersion.id ? activeVersion : v));
        }
    }, [activeVersion]);

    // Update doc 3 seconds after the last doc and versions change
    useEffect(() => {
        if (JSON.stringify(doc) === JSON.stringify(savedDocSnapshot) && JSON.stringify(versions) === JSON.stringify(savedVersionsSnapshot)) {
            setIsUpdating(false);
            return;
        }

        setIsUpdating(true);
        updateDocTimeout = setTimeout(async () => {
            await updateDoc();
            setIsUpdating(false);
        }, 3000);

        return () => clearTimeout(updateDocTimeout);
    }, [doc, versions]);

    // Modify doc name on type
    const updateDocName = (e) => {
        setDoc(prevDoc => ({
            ...prevDoc,
            name: e.target.value
        }));
    }

    // Create a new version
    const createVersion = async () => {
        const newVersion = {
            version: `Version ${versions.length + 1}`,
            content: mainVersion.content,
            is_main: false,
            doc: doc.id
        }

        try {
            const res = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/versions/create`, {
                method: 'POST',
                credentials: 'include',
                headers: {
                    'Content-Type': 'application/json',
                    'X-CSRFToken': getCookie('csrftoken')
                },
                body: JSON.stringify(newVersion)
            });
            const data = await res.json();

            if (res.ok) {
                console.log('Version created');
                console.log(data);

                newVersion.id = data.version.id;

                setVersions(prevVersions => [...prevVersions, newVersion]);
                setVersionStates(prevVersionState => ({
                    ...prevVersionState,
                    [newVersion.id]: 'edit'
                }));
                setActiveVersion(newVersion);
            } else {
                console.error('Failed to create version');
                console.log(data.message);
            }
        } catch (error) {
            console.error('Failed to create version');
            console.log(error);
        };
    }

    // Modify a version on type
    const updateVersion = (e, versionId) => {
        // Update version name
        if (e.target.name === 'version') {
            setVersions(prevVersions => prevVersions.map(v => v.id === versionId ? {...v, version: e.target.value} : v));

            // Update active version name if it is the same version
            if (activeVersion.id === versionId) {
                setActiveVersion(prevActiveVersion => ({
                    ...prevActiveVersion,
                    version: e.target.value
                }));
            }
            return;
        }

        // Update version content
        setVersions(prevVersions => prevVersions.map(v => v.id === versionId ? {...v, content: e.target.value} : v));

        // Update active version content if it is the same version
        if (activeVersion.id === versionId) {
            setActiveVersion(prevActiveVersion => ({
                ...prevActiveVersion,
                content: e.target.value
            }));
        }
    } 

    // Delete a version
    const deleteVersion = (versionId) => {
        setDeletedVersionIds(prevDeletedVersionIds => [...prevDeletedVersionIds, versionId]);
        setVersions(prevVersions => prevVersions.filter(v => v.id !== versionId));
        setVersionStates(prevVersionState => {
            delete prevVersionState[versionId];
            return prevVersionState;
        });
        if (activeVersion.id === versionId) {
            setActiveVersion(mainVersion);
        }
    }

    // Add tag
    const addTag = (e) => {
        e.preventDefault();
        const tag = e.target.tags.value;
        if (tag) {
            setDoc(prevDoc => ({
                ...prevDoc,
                tags: [...prevDoc.tags, tag]
            }));
            e.target.reset();
        }
    }

    // Delete tag
    const deleteTag = (tag) => {
        setDoc(prevDoc => ({
            ...prevDoc,
            tags: prevDoc.tags.filter(t => t !== tag)
        }));
    }

    // Update doc
    const updateDoc = async () => {
        try {
            const res = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/docs/update/${doc.id}`, {
                method: 'PUT',
                credentials: 'include',
                headers: {
                    'Content-Type': 'application/json',
                    'X-CSRFToken': getCookie('csrftoken')
                },
                body: JSON.stringify({
                    ...doc,
                    tags: doc.tags.join(','),
                    versions: versions,
                    deleted_versions: deletedVersionIds
                })
            });
            const data = await res.json();

            if (res.ok) {
                console.log('Doc updated');
                console.log(data);

                // Update doc in the list
                setDocs(docs.map(d => d.id === doc.id ? data.doc : d));

                // Add new tags into the workspace tags
                const newTags = data.doc.tags.filter(tag => !workspace.tags.includes(tag));
                setWorkspace(prevWorkspace => ({
                    ...prevWorkspace,
                    tags: [...prevWorkspace.tags, ...newTags]
                }));

                // Reset deleted version ids
                setDeletedVersionIds([]);

                // Update saved snapshots
                setSavedDocSnapshot(data.doc);
                setSavedVersionsSnapshot(data.versions);

                // Updated successfully
                setUpdatedSuccessfully(true);
            } else {
                console.error('Failed to update doc');
                console.log(data.message);
                setUpdatedSuccessfully(false);
            }
        } catch (error) {
            console.error('Failed to update doc');
            console.log(error);
            setUpdatedSuccessfully(false);
        }
    }

    // When closing the modal by hitting cancel, check if there is any changes to the doc.
    // If there is, ask the user if they want to save the changes.
    const closeModal = () => {
        // Notify users when they close doc while the doc is not saved
        if (isUpdating || !updatedSuccessfully) {
            if (!confirm('Do you want to leave this page? Any unsaved changes will be lost.')) {
                return;
            };
        }

        router.push(pathname);
        setIsOpen(false);
    }

    return (
        <CustomDialog isOpen={isOpen} setIsOpen={setIsOpen} title={doc.name} size="7xl" onClose={closeModal}>
            <div className="grid gap-4 mb-4 grid-cols-2">
                
                {/* Contributors */}
                {doc.created_by || doc.contributors.length > 0 ?
                <div className="col-span-2">
                    {doc.created_by ? 
                    <div className="text-xs text-gray-500">
                        Created by {doc.created_by} on {new Date(doc.created_at).toLocaleString()}.
                    </div>: null}
                    {doc.contributors.length > 0 ?
                    <div className="text-xs text-gray-500">
                        Contributors: {doc.contributors.join(', ')}
                    </div> : null
                    }
                </div> : null}

                {/* Share link */}
                <ShareLink docUrl={docUrl}/>
                
                {/* Name */}
                <div className="col-span-2">
                    <label htmlFor="name" className="block mb-2 text-sm font-medium text-gray-900">Name</label>
                    <input 
                        type="text" 
                        name="name" 
                        className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5" 
                        placeholder="Document name" 
                        value={doc.name}
                        onChange={updateDocName}
                        required/>
                </div>

                {/* Content */}
                <div className="col-span-2">
                    {versions.length > 0 ?
                    <div>
                        <div className="flex flex-row justify-between">
                            <label htmlFor="description" className="mb-2 text-sm font-medium text-gray-900 flex flex-fow gap-2 my-auto">
                                <span className="my-auto">Content (Markdown supported) {!activeVersion.is_main ? " -" : null}</span>
                                {!activeVersion.is_main ? <VersionName version={activeVersion} updateVersion={setActiveVersion}/> : null}
                            </label>
                            {/* Number of version badge */}
                            <span className="mb-2 inline-flex items-center rounded-md bg-blue-50 px-2 py-1 text-xs font-medium text-blue-700 ring-1 ring-inset ring-blue-700/10">
                                {versions.length} {versions.length === 1 ? "version" : "versions"} found
                            </span>
                        </div>
                        <div className="flex flex-row justify-between">

                            {/* Edit/Preview button */}
                            <div className="inline-flex rounded-md shadow-sm mb-2 w-fit" role="group">
                                <button 
                                    type="button" 
                                    className={`inline-flex items-center px-4 py-2 text-sm font-medium ${versionStates[activeVersion.id] === 'edit' ? "text-blue-700 bg-gray-100" : "text-gray-900 bg-white"} border border-gray-200 rounded-s-lg hover:bg-gray-100 hover:text-blue-700 focus:z-10 focus:ring-2 focus:ring-blue-700 focus:text-blue-700`}
                                    onClick={() => setVersionStates(prevVersionState => ({
                                        ...prevVersionState,
                                        [activeVersion.id]: 'edit'
                                    }))}>
                                    <i className="fa-solid fa-pen me-2"></i>
                                    Edit
                                </button>
                                <button 
                                    type="button" 
                                    className={`inline-flex items-center px-4 py-2 text-sm font-medium  ${versionStates[activeVersion.id] === 'preview' ? "text-blue-700 bg-gray-100" : "text-gray-900 bg-white"} border border-gray-200 rounded-e-lg hover:bg-gray-100 hover:text-blue-700 focus:z-10 focus:ring-2 focus:ring-blue-700 focus:text-blue-700`}
                                    onClick={() => setVersionStates(prevVersionState => ({
                                        ...prevVersionState,
                                        [activeVersion.id]: 'preview'
                                    }))}>
                                    <i className="fa-solid fa-eye me-2"></i>
                                    Preview
                                </button>

                                {/* Saving/saved spinner */}
                                {isUpdating ? <div className="text-sm flex flex-row items-center ml-3 gap-2 text-gray-500">
                                    <span className="loading loading-spinner loading-xs"></span>
                                    Saving...
                                </div> :
                                updatedSuccessfully ?
                                <div className="text-sm flex flex-row items-center ml-3 gap-2 text-green-700">
                                    <CheckIcon className="h-4 w-4 font-semibold"/>
                                    Saved
                                </div> : 
                                <div className="text-sm flex flex-row items-center ml-3 gap-2 text-red-700">
                                    <XMarkIcon className="h-4 w-4 font-semibold"/>
                                    Failed to save. Check console for errors.
                                </div>}
                            </div>

                            <div className="flex flex-row gap-3 mb-2">
                                {/* Version create button */}
                                <button
                                    type="button"
                                    className={`${buttonStyle["primary-sm"]} h-fit my-auto flex flex-row`}
                                    onClick={createVersion}
                                >
                                    <PlusIcon className="h-4 w-4 me-1 my-auto"/>
                                    Create version
                                </button>

                                {/* Version select tag */}
                                <div className="flex flex-row gap-1">
                                    <label htmlFor="active-version" className="block text-sm font-medium leading-6 text-gray-900 my-auto">
                                        Version:
                                    </label>
                                    <select
                                        id="active-version"
                                        name="active-version"
                                        className="block w-full rounded-md border-0 pl-3 pr-10 text-gray-900 ring-1 ring-inset ring-gray-300 focus:ring-2 focus:ring-indigo-600 sm:text-sm sm:leading-6"
                                        value={activeVersion.id}
                                        onChange={(e) => setActiveVersion(versions.find(v => v.id === parseInt(e.target.value)))}
                                    >
                                        {versions.map(version => (
                                            <option key={version.id} value={version.id}>{version.version}</option>
                                        ))}
                                    </select>
                                </div>

                                {/* Version menu */}
                                {!activeVersion.is_main ? <VersionMenu doc={doc} version={activeVersion} deleteVersion={deleteVersion}/> : null}
                            </div>
                        </div>

                        {versions.map(version => (
                            <Version 
                                key={version.id}
                                version={version}
                                updateVersion={(e) => updateVersion(e, version.id)}
                                state={versionStates[version.id]}
                                show={version.id === activeVersion.id}/>
                        ))}
                    </div>: "Loading content..."}

                    {/* Tags */}
                    <div className="mt-4">
                        <label htmlFor="tags" className="block mb-2 text-sm font-medium text-gray-900">Tags</label>
                        <form onSubmit={addTag}>
                            <input 
                                type="text" 
                                name="tags" 
                                className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5" 
                                placeholder="Tags" />
                        </form>
                        {/* Render existing tags */}
                        <div className="mt-2">
                            {doc.tags.map(tag => (
                            <span key={tag} className="inline-flex items-center px-2.5 py-1.5 text-xs font-medium bg-gray-100 text-gray-900 rounded-sm me-2">
                                {tag}
                                <button 
                                    type="button" 
                                    className="text-gray-400 hover:text-gray-600"
                                    onClick={() => deleteTag(tag)}>
                                    <i className="fa-solid fa-xmark ms-2"></i>
                                </button>
                            </span>
                            ))}
                        </div>
                    </div>
                </div>
            </div>
        </CustomDialog>
    )
}
