"use client";

import { useState, useContext } from "react";

import { getCookie } from "@/helper";
import { DocsContext } from "../../DocList";
import CustomDialog from "@/components/CustomDialog";

export default function DeleteDocDialog({ isOpen, setIsOpen, doc }) {
    const {docs, setDocs} = useContext(DocsContext);
    const [isDeleting, setIsDeleting] = useState(false);

    // Delete doc
    const deleteDoc = async (e) => {
        e.preventDefault();
        setIsDeleting(true);

        try {
            const res = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/docs/delete/${doc.id}`, {
                method: 'DELETE',
                credentials: 'include',
                headers: {
                    'Content-Type': 'application/json',
                    'X-CSRFToken': getCookie('csrftoken')
                }
            });
            const data = await res.json();
            setIsDeleting(false);

            if (res.ok) {
                console.log('Doc deleted');
                console.log(data);

                // Close dialog
                setIsOpen(false);

                // Remove doc from the list
                setDocs(docs.filter(d => d.id !== doc.id));
            } else {
                console.error('Failed to delete doc');
                console.log(data.errors);
            }
        } catch (error) {
            setIsDeleting(false);
            console.error('Failed to delete doc');
            console.log(error);
        }
    }

    return (
        <CustomDialog isOpen={isOpen} onClose={() => setIsOpen(false)} title="Delete doc" size="lg">
            <div className="block py-2 text-sm">Confirm deleting doc <strong>{doc.name}</strong></div>
            <form onSubmit={deleteDoc}>
                <button 
                    type="submit" 
                    className={`text-xs text-white w-full bg-red-500 hover:bg-red-700 rounded-md py-2 px-4 mt-4`}>
                    {isDeleting ? 'Deleting...' : 'Delete'}
                </button>
            </form>
        </CustomDialog>
    )
}