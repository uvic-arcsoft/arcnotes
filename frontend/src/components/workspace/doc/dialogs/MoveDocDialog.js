"use client";

import { useContext, useState, useEffect } from "react";

import { getCookie } from "@/helper";
import { RootWorkspacesContext } from "@/components/layouts/AppRootLayout";
import { WorkspaceContext } from "@/app/workspace/[workspaceId]/page";
import { DocListsContext } from "../../DocList";
import { DocsContext } from "../../DocList";
import CustomDialog from "@/components/CustomDialog";
import WarningAlert from "@/components/alerts/WarningAlert";
import { labelStyle, inputStyle, helpTextStyle, selectStyle } from "@/styles/form";
import { buttonStyle } from "@/styles/buttons";

export default function MoveDocDialog({ isOpen, setIsOpen, doc }) {
    const {workspaces} = useContext(RootWorkspacesContext);
    const {workspace} = useContext(WorkspaceContext);
    const {docLists, setDocLists} = useContext(DocListsContext);
    const [workspaceOptions, setWorkspaceOptions] = useState(workspaces);
    const [selectedWorkspace, setSelectedWorkspace] = useState(workspace);
    const [docListOptions, setDocListOptions] = useState(docLists);
    const [displayWarning, setDisplayWarning] = useState(false);

    useEffect(() => {
        // Update the currently selected workspace if it is the current workspace
        // and there is a change to the current workspace
        if (workspace.id === selectedWorkspace.id) {
            setSelectedWorkspace(workspace);
        } 
    }, [workspace]);

    useEffect(() => {
        /* Only display workspace options that contain the author and all of the contributors of the doc */
        // Filter workspaces
        const filteredWorkspaces = workspaces.filter(w => {
            return (
                ["public", "authenticated"].includes(w.sharing_option) ||
                w.sharing_option === "individuals" && doc.contributors.every(c => w.members.some(u => u.username === c))
            );
        });

        setWorkspaceOptions(filteredWorkspaces);
        setSelectedWorkspace(filteredWorkspaces[0]);
    }, [workspaces, doc]);

    useEffect(() => {
        /* Only display doc lists that belong to the selected workspace */
        if (selectedWorkspace.id === workspace.id) {
            setDocListOptions(docLists);
            return;
        }
        setDocListOptions(selectedWorkspace.doc_lists);
    }, [selectedWorkspace.id, docLists, workspace.id]);

    useEffect(() => {
        // Display a warning if the selected workspace contains different users from the current workspace
        if (workspace.members.length !== selectedWorkspace.members.length
            || !workspace.members.every((member, index) => member.username === selectedWorkspace.members[index].username)) {
            setDisplayWarning(true);
            return;
        }
        setDisplayWarning(false);
    }, [selectedWorkspace]);
    
    // Move doc
    const moveDoc = async (e) => {
        e.preventDefault();
        const formData = new FormData(e.target);
        const newDocName = formData.get('name');
        const newWorkspaceId = selectedWorkspace.id;
        const newDocListId = formData.get('doc-list');

        try {
            const res = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/docs/move/${doc.id}`, {
                method: 'PUT',
                credentials: 'include',
                headers: {
                    'Content-Type': 'application/json',
                    'X-CSRFToken': getCookie('csrftoken')
                },
                body: JSON.stringify({
                    new_doc_name: newDocName,
                    new_workspace_id: newWorkspaceId,
                    new_doc_list_id: newDocListId
                })
            });
            const data = await res.json();

            if (res.ok) {
                console.log('Doc moved');
                console.log(data);

                // Update the doc lists
                setDocLists(data.doc_lists);

                // Close dialog
                setIsOpen(false);
            } else {
                console.error('Failed to move doc');
                console.log(data.message);
            }
        } catch (error) {
            console.error('Failed to move doc');
            console.log(error);
        }
    }

    return (
        <CustomDialog isOpen={isOpen} onClose={() => setIsOpen(false)} title="Move doc" size="lg">
            <form onSubmit={moveDoc}>
                {/* Doc name */}
                <div>
                    <label htmlFor="new-doc-name" className={labelStyle.neutral}>
                        Doc name
                    </label>
                    <div className="mt-2">
                        <input
                        type="text"
                        name="name"
                        id="new-doc-name"
                        className={inputStyle.neutral}
                        defaultValue={doc.name}
                        placeholder="New doc's name"
                        />
                    </div>
                    <p className={helpTextStyle.neutral} id="email-description">
                        The name of the doc at the new destination.
                    </p>
                </div>

                {/* Workspace */}
                <div className="mt-3">
                    {displayWarning ? <WarningAlert 
                        title="New access to doc" 
                        message="The workspace that you are moving the doc to contains different users from this workspace. Moving this doc might result in new users having access to the doc. If you're ok with this, feel free to proceed."/>
                    : null}
                    <label htmlFor="new-workspace" className={`${labelStyle.neutral} mt-3`}>
                        Workspace
                    </label>
                    <select
                        id="new-workspace"
                        name="workspace"
                        className={selectStyle.neutral}
                        defaultValue={workspace.name}
                        onChange={(e) => {
                            setSelectedWorkspace(workspaceOptions.find(w => w.id === parseInt(e.target.value)));
                        }}
                    >
                        {workspaceOptions.map(w => (
                            <option key={w.id} value={w.id}>{w.name}</option>
                        ))}
                    </select>
                </div>

                {/* Doc list */}
                <div className="mt-3">
                    <label htmlFor="new-doc-list" className={labelStyle.neutral}>
                        Doc list
                    </label>
                    <select
                        id="new-doc-list"
                        name="doc-list"
                        className={selectStyle.neutral}
                        defaultValue={docLists[0].id}
                    >
                        {docListOptions.map(dl => (
                            <option key={dl.id} value={dl.id}>{dl.name}</option>
                        ))}
                    </select>
                </div>

                {/* Submit btrn */}
                <button 
                    type="submit" 
                    className={`${buttonStyle.primary} w-full mt-4`}>
                    Move
                </button>
            </form>
        </CustomDialog>
    )
}
