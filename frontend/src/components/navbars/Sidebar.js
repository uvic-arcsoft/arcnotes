"use client";

import Link from 'next/link';
import { useRouter, usePathname } from 'next/navigation';

import { Fragment, useState, useContext, useRef, useEffect } from 'react'
import { Dialog, Transition, Menu } from '@headlessui/react'
import { XMarkIcon, EllipsisHorizontalIcon } from '@heroicons/react/24/outline'

import useOutsideClick from '@/hooks/useOutsideClick';
import CustomDialog from '../CustomDialog';
import { SidebarContext, RootWorkspacesContext, UserContext } from '../layouts/AppRootLayout';
import { classNames, getCookie } from '@/helper';

export default function Sidebar () {
    const router = useRouter();

    const { sidebarOpen, setSidebarOpen } = useContext(SidebarContext);
    const { workspaces, setWorkspaces } = useContext(RootWorkspacesContext);
    const { user } = useContext(UserContext);
    const [yourWorkspaces, setYourWorkspaces] = useState([]);
    const [sharedWorkspaces, setSharedWorkspaces] = useState([]);
    const [publicWorkspaces, setPublicWorkspaces] = useState([]);
    const [authenticatedWorkspaces, setAuthenticatedWorkspaces] = useState([]);
    const [selectedWorkspace, setSelectedWorkspace] = useState(null);

    useEffect(() => {
        setYourWorkspaces(workspaces.filter(ws => ws.created_by.username === user.email));
        setSharedWorkspaces(workspaces.filter(ws => ws.sharing_option === "individuals" && ws.created_by.username !== user.email));
        setPublicWorkspaces(workspaces.filter(ws => ws.sharing_option === "public" && ws.created_by.username !== user.email));
        setAuthenticatedWorkspaces(workspaces.filter(ws => ws.sharing_option === "authenticated" && ws.created_by.username !== user.email));
    }, [workspaces]);

    return (
        <>
            <Transition.Root show={sidebarOpen} as={Fragment}>
                <Dialog as="div" className="relative z-50 lg:hidden" onClose={setSidebarOpen}>
                    <Transition.Child
                    as={Fragment}
                    enter="transition-opacity ease-linear duration-300"
                    enterFrom="opacity-0"
                    enterTo="opacity-100"
                    leave="transition-opacity ease-linear duration-300"
                    leaveFrom="opacity-100"
                    leaveTo="opacity-0"
                    >
                    <div className="fixed inset-0 bg-gray-900/80" />
                    </Transition.Child>

                    <div className="fixed inset-0 flex">
                    <Transition.Child
                        as={Fragment}
                        enter="transition ease-in-out duration-300 transform"
                        enterFrom="-translate-x-full"
                        enterTo="translate-x-0"
                        leave="transition ease-in-out duration-300 transform"
                        leaveFrom="translate-x-0"
                        leaveTo="-translate-x-full"
                    >
                        <Dialog.Panel className="relative mr-16 flex w-full max-w-xs flex-1">
                        <Transition.Child
                            as={Fragment}
                            enter="ease-in-out duration-300"
                            enterFrom="opacity-0"
                            enterTo="opacity-100"
                            leave="ease-in-out duration-300"
                            leaveFrom="opacity-100"
                            leaveTo="opacity-0"
                        >
                            <div className="absolute left-full top-0 flex w-16 justify-center pt-5">
                            <button type="button" className="-m-2.5 p-2.5" onClick={() => setSidebarOpen(false)}>
                                <span className="sr-only">Close sidebar</span>
                                <XMarkIcon className="h-6 w-6 text-white" aria-hidden="true" />
                            </button>
                            </div>
                        </Transition.Child>
                        {/* Sidebar component, swap this element with another sidebar if you like */}
                        <div className="flex grow flex-col gap-y-5 overflow-y-auto bg-white px-6 pb-4">
                            <div className="flex h-16 shrink-0 items-center">
                                <Link href="/" className='font-semibold text-2xl text-gray-700'>ARCNotes</Link>
                            </div>
                            <nav className="flex flex-1 flex-col">
                                <ul role="list" className="flex flex-1 flex-col gap-y-7">
                                    {/* Your workspaces */}
                                    <li>
                                        <div className="text-xs font-semibold leading-6 text-gray-400">Your workspaces</div>
                                        <ul role="list" className="-mx-2 mt-2 space-y-1">
                                            <CreateWorkspaceBtn/>
                                            {yourWorkspaces.map((ws) => <WorkspaceItem 
                                                key={ws.id}
                                                workspace={ws}
                                                selected={selectedWorkspace === ws.id}
                                                onClick={() => setSelectedWorkspace(ws.id)}/>)}
                                        </ul>
                                    </li>

                                    {/* Shared workspaces */}
                                    {sharedWorkspaces.length > 0 && (
                                    <li>
                                        <div className="text-xs font-semibold leading-6 text-gray-400">Shared with you</div>
                                        <ul role="list" className="-mx-2 mt-2 space-y-1">
                                            {sharedWorkspaces.map((ws) => <WorkspaceItem 
                                                key={ws.id}
                                                workspace={ws}
                                                selected={selectedWorkspace === ws.id}
                                                onClick={() => setSelectedWorkspace(ws.id)}/>)}
                                        </ul>
                                    </li>)}

                                    {/* Public workspaces */}
                                    {publicWorkspaces.length > 0 && (
                                    <li>
                                        <div className="text-xs font-semibold leading-6 text-gray-400">Public workspaces</div>
                                        <ul role="list" className="-mx-2 mt-2 space-y-1">
                                            {publicWorkspaces.map((ws) => <WorkspaceItem 
                                                key={ws.id}
                                                workspace={ws}
                                                selected={selectedWorkspace === ws.id}
                                                onClick={() => setSelectedWorkspace(ws.id)}/>)}
                                        </ul>
                                    </li>)}

                                    {/* Authenticated workspaces */}
                                    {authenticatedWorkspaces.length > 0 && (
                                    <li>
                                        <div className="text-xs font-semibold leading-6 text-gray-400">Authenticated workspaces</div>
                                        <ul role="list" className="-mx-2 mt-2 space-y-1">
                                            {authenticatedWorkspaces.map((ws) => <WorkspaceItem 
                                                key={ws.id}
                                                workspace={ws}
                                                selected={selectedWorkspace === ws.id}
                                                onClick={() => setSelectedWorkspace(ws.id)}/>)}
                                        </ul>
                                    </li>)}
                                </ul>
                            </nav>
                        </div>
                        </Dialog.Panel>
                    </Transition.Child>
                    </div>
                </Dialog>
            </Transition.Root>

            {/* Static sidebar for desktop */}
            <div className="hidden lg:fixed lg:inset-y-0 lg:z-50 lg:flex lg:w-72 lg:flex-col">
                {/* Sidebar component, swap this element with another sidebar if you like */}
                <div className="flex grow flex-col gap-y-5 overflow-y-auto border-r border-gray-200 bg-white px-6 pb-4">
                    <div className="flex h-16 shrink-0 items-center">
                        <Link href="/" className='font-semibold text-2xl text-gray-700'>ARCNotes</Link>
                    </div>
                    <nav className="flex flex-1 flex-col">
                        <ul role="list" className="flex flex-1 flex-col gap-y-7">
                            {/* Your Workspaces */}
                            <li>
                                <div className="text-xs font-semibold leading-6 text-gray-400">Your workspaces</div>
                                <ul role="list" className="-mx-2 mt-2 space-y-1">
                                    <CreateWorkspaceBtn/>
                                    {yourWorkspaces.map((ws) => <WorkspaceItem 
                                        key={ws.id}
                                        workspace={ws}
                                        selected={selectedWorkspace === ws.id}
                                        onClick={() => setSelectedWorkspace(ws.id)}/>)}
                                </ul>
                            </li>

                            {/* Shared workspaces */}
                            {sharedWorkspaces.length > 0 && (
                            <li>
                                <div className="text-xs font-semibold leading-6 text-gray-400">Shared with you</div>
                                <ul role="list" className="-mx-2 mt-2 space-y-1">
                                    {sharedWorkspaces.map((ws) => <WorkspaceItem 
                                        key={ws.id}
                                        workspace={ws}
                                        selected={selectedWorkspace === ws.id}
                                        onClick={() => setSelectedWorkspace(ws.id)}/>)}
                                </ul>
                            </li>)}

                            {/* Public workspaces */}
                            {publicWorkspaces.length > 0 && (
                            <li>
                                <div className="text-xs font-semibold leading-6 text-gray-400">Public workspaces</div>
                                <ul role="list" className="-mx-2 mt-2 space-y-1">
                                    {publicWorkspaces.map((ws) => <WorkspaceItem 
                                        key={ws.id}
                                        workspace={ws}
                                        selected={selectedWorkspace === ws.id}
                                        onClick={() => setSelectedWorkspace(ws.id)}/>)}
                                </ul>
                            </li>)}

                            {/* Authenticated workspaces */}
                            {authenticatedWorkspaces.length > 0 && (
                            <li>
                                <div className="text-xs font-semibold leading-6 text-gray-400">Authenticated workspaces</div>
                                <ul role="list" className="-mx-2 mt-2 space-y-1">
                                    {authenticatedWorkspaces.map((ws) => <WorkspaceItem 
                                        key={ws.id}
                                        workspace={ws}
                                        selected={selectedWorkspace === ws.id}
                                        onClick={() => setSelectedWorkspace(ws.id)}/>)}
                                </ul>
                            </li>)}
                        </ul>
                    </nav>
                </div>
            </div>
        </>
    )
}

const CreateWorkspaceBtn = () => {
    const [state, setState] = useState('view');
    const {workspaces, setWorkspaces} = useContext(RootWorkspacesContext);
    const workspaceFormRef = useRef(null);

    // Close the workspace form when clicked outside
    useOutsideClick(workspaceFormRef, () => setState('view'));

    const createWorkspace = async (e) => {
        e.preventDefault();
        const res = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/workspaces/create`, {
            method: 'POST',
            credentials: 'include',
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': getCookie("csrftoken")
            },
            body: JSON.stringify({
                'name': e.target.name.value
            }),
        });
        const data = await res.json();

        if (res.ok) {
            console.log('Workspace created');
            console.log(data);

            // Render the new workspace
            setWorkspaces([...workspaces, data.workspace]);
            
            // Clear and close form
            e.target.reset();
            setState('view');
        } else {
            console.error('Failed to create workspace');
            console.log(data.errors);
        }
    }

    if (state === 'edit') {
        return (
            <li className="flex items-center p-2 text-gray-700 rounded-lg group cursor-pointer">
                <form 
                    className="max-w-none w-full" 
                    onSubmit={createWorkspace}
                    ref={workspaceFormRef}>
                    <input 
                        type="text"
                        name="name"
                        className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 " 
                        placeholder="Workspace title" 
                        required/>
                        <div className="mt-1 flex flex-row gap-1 justify-end">
                            <button 
                                type="button" 
                                className="px-3 py-2 text-xs font-medium text-center text-white bg-red-700 rounded-lg hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-red-300"
                                onClick={() => setState('view')}>Cancel</button>
                            <button type="submit" className="px-3 py-2 text-xs font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300">Save</button>
                        </div>
                </form>
            </li>
        )
    }

    return (
        <li
        className="flex items-center p-2 text-gray-700 hover:text-indigo-600 rounded-lg hover:bg-gray-50 font-semibold leading-6 text-sm group cursor-pointer" 
        onClick={() => setState('edit')}>
            <i className="fa-solid fa-plus"></i>
            <span className="ms-3">Create workspace</span>
        </li>
    )
}

const WorkspaceItem = ({ workspace, selected=false, onClick }) => {
    return (
        <li className="cursor-pointer" onClick={onClick}>
            <span
            className={classNames(
                selected
                ? 'bg-gray-50 text-indigo-600'
                : 'text-gray-700 hover:text-indigo-600 hover:bg-gray-50',
                'group flex rounded-md p-2 text-sm leading-6 font-semibold'
            )}
            >
                <Link href={`/workspace/${workspace.id}`} className='flex gap-x-3 grow'>
                    <span
                        className={classNames(
                        selected
                            ? 'text-indigo-600 border-indigo-600'
                            : 'text-gray-400 border-gray-200 group-hover:border-indigo-600 group-hover:text-indigo-600',
                        'flex h-6 w-6 shrink-0 items-center justify-center rounded-lg border text-[0.625rem] font-medium bg-white'
                        )}
                    >
                        {workspace.name.charAt(0)}
                    </span>
                    <span className="truncate">{workspace.name}</span>
                </Link>
                <WorkspaceDropdown workspace={workspace}/>
            </span>
        </li>
    )
}

const WorkspaceDropdown = ({ workspace }) => {
    const workspaceNameLowercase = workspace.name.toLowerCase().replace(/\s+/g, '');

    const router = useRouter();
    const pathname = usePathname();
    const {workspaces, setWorkspaces} = useContext(RootWorkspacesContext);
    const {user} = useContext(UserContext);

    const [isDeleteDialogOpen, setIsDeleteDialogOpen] = useState(false);
    const [deleteInput, setDeleteInput] = useState('');
    const [isDeleting, setIsDeleting] = useState(false);

    const deleteWorkspace = async (e) => {
        e.preventDefault();
        
        // Do nothing if the confirm input is incorrect
        if (deleteInput !== workspaceNameLowercase) return;
        
        setIsDeleting(true);

        const res = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/workspaces/delete/${workspace.id}`, {
            method: 'DELETE',
            credentials: 'include',
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': getCookie("csrftoken")
            },
        });
        const data = await res.json();

        if (res.ok) {
            console.log('Workspace deleted');
            console.log(data);

            // Remove the workspace from the list
            setWorkspaces(workspaces.filter(w => w.id !== workspace.id));

            // Close delete dialog
            setIsDeleteDialogOpen(false);

            // If current workspace is deleted, redirect to the first workspace
            if (pathname === `/workspace/${workspace.id}`) {
                router.push(`/workspace/${workspaces[0].id}`);
            }
        } else {
            console.error('Failed to delete workspace');
            console.log(data.errors);
        }

        setIsDeleting(false);
    }

    return (
        <>
            <Menu as="span" className="relative">
                <Menu.Button>
                    <EllipsisHorizontalIcon className="menu-icon hidden group-hover:block w-5 h-5 ml-auto my-auto rounded-md duration-75 hover:text-black hover:bg-gray-200"/>
                </Menu.Button>
                <Transition
                    as={Fragment}
                    enter="transition ease-out duration-100"
                    enterFrom="transform opacity-0 scale-95"
                    enterTo="transform opacity-100 scale-100"
                    leave="transition ease-in duration-75"
                    leaveFrom="transform opacity-100 scale-100"
                    leaveTo="transform opacity-0 scale-95"
                >
                    <Menu.Items className="absolute right-0 w-60 z-10 mt-2.5 py-2 origin-top-right rounded-sm bg-white shadow-lg ring-1 ring-gray-900/5 focus:outline-none">
                        {workspace.created_by.username === user.email &&
                        <>
                            <Menu.Item>
                                <span className='block px-3 py-1 text-sm text-center font-semibold leading-6 text-gray-600 mb-3'>{workspace.name}</span>
                            </Menu.Item>
                            <Menu.Item onClick={() => setIsDeleteDialogOpen(true)}>
                            {({ active }) => (
                                <span
                                    className={classNames(
                                        active ? 'bg-gray-100' : '',
                                        'block px-3 py-1 text-sm leading-6 text-gray-900 font-medium cursor-pointer'
                                    )}
                                    >
                                    Delete workspace
                                </span>
                            )}
                            </Menu.Item>
                        </>}
                    </Menu.Items>
                </Transition>
            </Menu>

            {/* Dialog for deleting a workspace */}
            <CustomDialog isOpen={isDeleteDialogOpen} onClose={() => setIsDeleteDialogOpen(false)} title="Delete workspace" size="lg">
                <div className="block py-2 text-sm">Deleting a workspace will delete all docs and tags associated with it. To confirm deleting this workspace, type <strong>{workspaceNameLowercase}</strong> below:</div>
                <form onSubmit={deleteWorkspace}>
                    <input 
                        type="text" 
                        className="block w-full border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 p-2.5" 
                        placeholder={`Type "${workspaceNameLowercase}"`}
                        value={deleteInput}
                        onChange={(e) => setDeleteInput(e.target.value)}
                        required
                    ></input>
                    <button 
                        type="submit" 
                        className={`text-xs text-white w-full ${deleteInput !== workspaceNameLowercase ? "bg-red-200" : "bg-red-500 hover:bg-red-700"} rounded-md py-2 px-4 mt-4`}
                        disabled={deleteInput !== workspaceNameLowercase}>
                        {isDeleting ? 'Deleting...' : 'Delete'}
                    </button>
                </form>
            </CustomDialog>
        </>
    )
}
