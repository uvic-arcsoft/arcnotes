"use client";

import { Fragment, useState, useContext, useRef } from "react";
import { Menu, Transition } from "@headlessui/react";

import { getCookie } from "@/helper";
import useOutsideClick from "@/hooks/useOutsideClick";
import CustomDialog from "../CustomDialog";
import { RootWorkspacesContext } from "../layouts/AppRootLayout";
import { WorkspaceContext, SelectedTagsContext } from "@/app/workspace/[workspaceId]/page";
import { UserContext } from "../layouts/AppRootLayout";

export default function WorkspaceNavbar({workspace}) {
    const { user } = useContext(UserContext);

    return (
        <>
        <nav className="bg-white border-gray-200 sticky top-0 left-0 right-0 border-b border-b-slate-300 px-8 py-4 z-30">
            <div className="max-w-screen-xl flex flex-wrap items-center justify-between mx-auto">
                <div className="flex items-center space-x-3 rtl:space-x-reverse">
                    <WorkspaceName/>
                </div>
                <div className="hidden w-full md:block md:w-auto" id="navbar-default">
                    <ul className="font-medium flex flex-col p-4 md:p-0 mt-4 border border-gray-100 rounded-lg bg-gray-50 md:flex-row md:space-x-8 rtl:space-x-reverse md:mt-0 md:border-0 md:bg-white list-none">
                        {/* Filter */}
                        <FilterBtn/>

                        {/* Share */}
                        {workspace.created_by && user.email === workspace.created_by.username ? <ShareBtn/> : null}
                    </ul>
                </div>
            </div>
        </nav>
        </>
    )
}

// Workspace editable name
const WorkspaceName = () => {
    const {workspaces, setWorkspaces} = useContext(RootWorkspacesContext);
    const {workspace, setWorkspace} = useContext(WorkspaceContext);
    const [state, setState] = useState('view');
    const nameInputRef = useRef(null);

    // Close the input field when clicked outside
    useOutsideClick(nameInputRef, () => setState('view'));

    // Update workspace name
    const updateWorkspace = async (e) => {
        e.preventDefault();
        const name = e.target.name.value;
        const res = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/workspaces/update/${workspace.id}`, {
            method: 'PUT',
            credentials: 'include',
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': getCookie('csrftoken')
            },
            body: JSON.stringify({name})
        });
        const data = await res.json();

        if (res.ok) {
            console.log('Workspace updated');
            console.log(data);

            // Update workspace for the app root layout
            setWorkspaces(workspaces.map(w => w.id === workspace.id ? data.workspace : w));

            // Update workspace for the entire workspace page
            setWorkspace(workspace => ({...workspace, name}));

            // Display the new name
            setState('view');
        } else {
            console.error('Failed to update workspace');
            console.log(data.errors);
        }
    }

    if (state === 'view') {
        return (
            <div className="hover:bg-gray-200 cursor-pointer px-4 py-2 rounded-md" onClick={() => setState('edit')}>
                <h2 className="self-center text-xl font-semibold whitespace-nowrap m-0">{workspace.name}</h2>
            </div>
        )
    }
    
    return (
        <form onSubmit={updateWorkspace}>
            <input 
                ref={nameInputRef}
                type="text" 
                name="name" 
                className="bg-gray-50 border border-gray-300 text-gray-900 text-xl font-semibold rounded-md block w-fit p-1" 
                defaultValue={workspace.name} 
                autoFocus />
        </form>
    )
}

// Filter button and dropdown
const FilterBtn = () => {
    const {workspace, setWorkspace} = useContext(WorkspaceContext);
    const {selectedTags, setSelectedTags} = useContext(SelectedTagsContext);
    const [isOpen, setIsOpen] = useState(false);
    const filterDropdownRef = useRef(null);

    // Close the dropdown when clicked outside
    useOutsideClick(filterDropdownRef, () => setIsOpen(false));

    return (
        <Menu as="li" className="h-fit my-auto relative" ref={filterDropdownRef}>
            <Menu.Button onClick={() => setIsOpen(!isOpen)} className="block py-2 px-3 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 cursor-pointer">
                <i className="fa-solid fa-arrow-up-short-wide me-2"></i>
                Filter
            </Menu.Button>
            <Transition
                    show={isOpen}
                    as={Fragment}
                    enter="transition ease-out duration-100"
                    enterFrom="transform opacity-0 scale-95"
                    enterTo="transform opacity-100 scale-100"
                    leave="transition ease-in duration-75"
                    leaveFrom="transform opacity-100 scale-100"
                    leaveTo="transform opacity-0 scale-95"
                >
                <Menu.Items 
                    as="div"
                    className="absolute top-[100%] right-0 bg-white rounded-lg shadow w-96 p-5">
                    <Menu.Item as="div">
                            <label className="text-sm font-semibold">Filter by tags:</label>
                            {/* Render all tags */}
                            <div className="mt-2 flex flex-row flex-wrap gap-2">
                                {workspace.tags && workspace.tags.map(tag => selectedTags.includes(tag) ? 
                                <span 
                                    key={tag}
                                    onClick={() => setSelectedTags(selectedTags.filter(t => t !== tag))}
                                    className="inline-flex items-center px-2.5 py-1.5 text-xs font-medium bg-gray-300 text-blue-700 rounded-sm cursor-pointer">
                                    {tag}
                                </span> :
                                <span 
                                    key={tag} 
                                    onClick={() => setSelectedTags([...selectedTags, tag])}
                                    className="inline-flex items-center px-2.5 py-1.5 text-xs font-medium bg-gray-100 hover:bg-gray-300 text-gray-900 hover:text-blue-700 rounded-sm cursor-pointer">
                                    {tag}
                                </span> )}
                            </div>
                    </Menu.Item>
                </Menu.Items>
            </Transition>
        </Menu>
    )
}

// const FilterBtn = () => {
//     const {workspace, setWorkspace} = useContext(WorkspaceContext);

//     return (
//         <li className="h-fit my-auto">
//             <button 
//                 className="block py-2 px-3 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 cursor-pointer"
//                 data-dropdown-toggle="filter-dropdown">
//                 <i className="fa-solid fa-arrow-up-short-wide me-2"></i>
//                 Filter
//             </button>
//             <div id="filter-dropdown" className="z-10 hidden bg-white divide-y divide-gray-100 rounded-lg shadow w-96 p-5">
//                 <label className="text-sm font-semibold">Filter by tags:</label>
//                 {/* Render all tags */}
//                 <div className="mt-2 flex flex-row flex-wrap gap-2">
//                     {workspace.tags && workspace.tags.map(tag => (
//                     <span key={tag} className="inline-flex items-center px-2.5 py-1.5 text-xs font-medium bg-gray-100 text-gray-900 rounded-sm cursor-pointer">
//                         {tag}
//                     </span>))}
//                 </div>
//             </div>
//         </li>
//     )
// }

// Share button and modal
const ShareBtn = () => {
    const {workspaces, setWorkspaces} = useContext(RootWorkspacesContext);
    const {workspace, setWorkspace} = useContext(WorkspaceContext);
    const [isOpen, setIsOpen] = useState(false);
    const [sharingOption, setSharingOption] = useState(workspace.sharing_option ? workspace.sharing_option : 'individuals');
    const [members, setMembers] = useState(workspace.members ? workspace.members : []);
    const [isUpdating, setIsUpdating] = useState(false);

    const addMember = async (e) => {
        e.preventDefault();
        const member = e.target.members.value;
        // Add member to the list if it doesn't exist
        if (members.find(m => m.username === member)) {
            return;
        }
        setMembers([...members, {id: members.length + 1, username: member}]);
        e.target.reset();
    }

    const deleteMember = (id) => {
        setMembers(members.filter(m => m.id !== id));
    }

    // Share workspace with others
    const saveSharingSettings = async () => {
        setIsUpdating(true);
        const res = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/workspaces/update_sharing_settings/${workspace.id}`, {
            method: 'PUT',
            credentials: 'include',
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': getCookie('csrftoken')
            },
            body: JSON.stringify({
                sharing_option: sharingOption,
                members: members.map(m => m.username)
            })
        });
        const data = await res.json();

        if (res.ok) {
            console.log('Workspace updated');
            console.log(data);

            // Update workspace and members
            setWorkspaces(workspaces.map(w => w.id === workspace.id ? data.workspace : w));
            setWorkspace(data.workspace);
            setSharingOption(data.workspace.sharing_option);
            setMembers(data.workspace.members);

            // Close modal
            setIsUpdating(false);
            setIsOpen(false);
        } else {
            console.error('Failed to update workspace');
            console.log(data.errors);
            setIsUpdating(false);
        }
    }

    // Workspace is loading
    if (!workspace.id) {
        return <h1>Loading...</h1>;
    }

    return (
        <li className="h-fit my-auto cursor-pointer">
            <span className="block py-2 px-3 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0" onClick={() => setIsOpen(true)}>
                <i className="fa-solid fa-share mr-2"></i>
                Share
            </span>

            {/* Share modal */}
            <CustomDialog isOpen={isOpen} onClose={() => setIsOpen(false)} title="Share workspace with others" size="2xl">
                <div className="grid gap-4 mb-4 grid-cols-2">
                    {/* Select sharing mode - private, authenticated, individuals, public */}
                    <div className="col-span-2">
                        <div>
                            <label htmlFor="sharingOption" className="block mb-2 text-sm font-medium text-gray-900">Sharing option:</label>
                            <form>
                                <select 
                                    name="sharingOption" 
                                    className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5"
                                    defaultValue={sharingOption}
                                    onChange={(e) => {
                                        setSharingOption(e.target.value);
                                    }}>
                                    <option value="private">Private - Only you have access to this workspace</option>
                                    <option value="authenticated">Authenticated - Every user in the system has access to this workspace</option>
                                    <option value="individuals">Individuals - Only explicitly selected users have access to this workspace</option>
                                    <option value="public">Public - Everyone has access to this workspace</option>
                                </select>
                            </form>
                        </div>
                    </div>

                    {/* Select members if sharing the workspace to individual users */}
                    {sharingOption === "individuals" ? 
                    <div className="col-span-2">
                        <div>
                            <label htmlFor="members" className="block mb-2 text-sm font-medium text-gray-900">Add member</label>
                            <form onSubmit={addMember}>
                                <input 
                                    type="text" 
                                    name="members" 
                                    className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5" 
                                    placeholder="Type an username (email)" />
                            </form>
                            {/* Render existing members */}
                            <div className="mt-2">
                                <div className="mb-2">
                                    {members.length > 0 ? <label className="text-sm">These members can view and edit docs in your workspace:</label> : null}
                                </div>
                                {members.map(member => (
                                    <span key={member.id} className="inline-flex items-center px-2.5 py-1.5 text-xs font-medium bg-gray-100 text-gray-900 rounded-sm me-2 mb-2">
                                        {member.username}
                                        <button 
                                            type="button" 
                                            className="text-gray-400 hover:text-gray-600"
                                            onClick={() => deleteMember(member.id)}>
                                            <i className="fa-solid fa-xmark ms-2"></i>
                                        </button>
                                    </span>
                                ))}
                            </div>
                        </div>
                    </div> : null}
                </div>
                {!isUpdating ? <button 
                    type="submit" 
                    className="text-white inline-flex items-center bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 me-2 text-center"
                    onClick={saveSharingSettings}>
                    <i className="fa-solid fa-floppy-disk me-2"></i>
                    Save
                </button> : 
                <button 
                    type="button" 
                    className="text-white bg-gray-400 cursor-not-allowed font-medium rounded-lg text-sm px-5 py-2.5 me-2 text-center">
                    <i className="fa-solid fa-spinner me-2"></i>
                    Saving...
                </button>}
                <button 
                    type="button" 
                    className="text-white bg-red-600 hover:bg-red-700 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center"
                    onClick={() => setIsOpen(false)}>
                    <i className="fa-solid fa-xmark me-2"></i>
                    Cancel
                </button>
            </CustomDialog>
        </li>
    )
}
