"use client";

import { useState, createContext, useContext, useEffect } from "react";
import { RootWorkspacesContext } from "@/components/layouts/AppRootLayout";
import ScrollableContainer from "@/components/containers/ScrollableContainer";
import WorkspaceNavbar from "@/components/navbars/WorkspaceNavbar";
import { DocListsDisplay } from "@/components/workspace/DocList";

export const WorkspaceContext = createContext();
export const SelectedTagsContext = createContext();

export default function Page({ params }) {
    const workspaceId = params.workspaceId;
    const {workspaces} = useContext(RootWorkspacesContext);
    
    // Find a workspace with an id from an array of workspaces
    const getWorkspaceById = (id, workspaces) => {
        for(let i = 0; i < workspaces.length; i++) {
            if (workspaces[i].id === id) {
                return workspaces[i];
            }
        }
    }

    const [workspace, setWorkspace] = useState(null);
    const [selectedTags, setSelectedTags] = useState([]);

    useEffect(() => {
        setWorkspace(getWorkspaceById(parseInt(workspaceId), workspaces));
    }, [workspaces]);

    if (!workspace) return <h1>Loading workspace...</h1>

    return (
        <WorkspaceContext.Provider value={{workspace, setWorkspace}}>
            <SelectedTagsContext.Provider value={{selectedTags, setSelectedTags}}>
                <WorkspaceNavbar workspace={workspace}/>
                
                {/* Docs list container */}
                <ScrollableContainer className="overflow-x-auto">
                    <div className="scrollable-container bg-slate-200 min-h-[90vh] p-6 min-w-full w-max">
                        <DocListsDisplay workspace={workspace}/>
                    </div>
                </ScrollableContainer>
            </SelectedTagsContext.Provider>
        </WorkspaceContext.Provider>
    );
}
