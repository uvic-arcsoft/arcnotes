export const labelStyle = {
    "neutral": "block text-sm font-medium leading-6 text-gray-900"
}

export const inputStyle = {
    "neutral": "block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
}

export const helpTextStyle = {
    "neutral": "mt-2 text-sm text-gray-500"
}

export const selectStyle = {
    "neutral": "mt-2 block w-full rounded-md border-0 py-1.5 pl-3 pr-10 text-gray-900 ring-1 ring-inset ring-gray-300 focus:ring-2 focus:ring-indigo-600 sm:text-sm sm:leading-6"
}