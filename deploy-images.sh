#!/bin/sh
# shellcheck disable=
# Install git if not installed
if ! [ -x "$(command -v git)" ]; then
    echo "Installing git..."
    apk update
    apk add git
fi

latest_tag=$(git describe --tags --abbrev=0)
echo "Latest tag: $latest_tag"

# No tag found
if [ -z "$latest_tag" ]; then
    # Get total number of commits
    num_commits=$(git rev-list HEAD --count)
    tag="dev-$num_commits"        

# Latest tag found
else
    # Get the number of commits since the last tag
    num_commits=$(git rev-list "$latest_tag"..HEAD --count)
    echo "Number of commits since last tag: $num_commits"
    
    # The last commit got tagged
    if [ "$num_commits" -eq 0 ]; then
        tag=$latest_tag

    # The last commit did not get tagged
    else
        tag="$latest_tag-dev-$num_commits"
    fi
fi

echo "Docker tag: $tag"

registry=$1
registry_repo="$registry/arcnotes"
registry_user=$2
registry_password=$3
app_url=$4

# Login to the registry
echo $registry_password | docker login -u $registry_user --password-stdin $registry

# Build the backend images
cd backend
docker build -f deployments/Dockerfile -t $registry_repo/arcnotes-backend:"$tag" .
docker build -f deployments/nginx/Dockerfile -t $registry_repo/arcnotes-nginx:"$tag" .

# Build the frontend image
cd ../frontend
echo "NEXT_PUBLIC_API_URL=$app_url/api" > .env.local
docker build -f Dockerfile.prod -t $registry_repo/arcnotes-frontend:"$tag" .
docker images

# Push the images to the registry
docker push $registry_repo/arcnotes-backend:"$tag"
docker push $registry_repo/arcnotes-nginx:"$tag"
docker push $registry_repo/arcnotes-frontend:"$tag"
