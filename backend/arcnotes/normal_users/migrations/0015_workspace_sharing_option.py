# Generated by Django 5.0.3 on 2024-08-08 22:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('normal_users', '0014_version_is_main'),
    ]

    operations = [
        migrations.AddField(
            model_name='workspace',
            name='sharing_option',
            field=models.CharField(blank=True, choices=[('private', 'private'), ('authenticated', 'authenticated'), ('individuals', 'individuals'), ('public', 'public')], default='individuals', max_length=20),
        ),
    ]
