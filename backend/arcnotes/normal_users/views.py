# pylint:
import json

from django.views import View
from django.http import JsonResponse
# from django.utils.decorators import method_decorator
# from django.views.decorators.csrf import csrf_exempt
from django.urls import reverse
from django.contrib.auth.models import User
from django.db.models import Q

from .models import Workspace, DocList, Doc, Version
from .forms import WorkspaceForm, DocListForm, DocForm

# Temporary disable CSRF protection for proof of concept
# @method_decorator(csrf_exempt, name='dispatch')
class WorkspaceAPIView(View):
    """
    Handle API calls to manipulate workspaces
    """
    def get(self, request, **kwargs):
        """
        Get 1 or all workspaces
        """
        # User must be authenticated to get workspaces
        if request.user.is_anonymous:
            return JsonResponse({
                "message": "You must be authenticated to get workspaces!"
            }, status=401)

        # Get a specific workspace
        # User with access to the workspace can view it
        if workspace_id := kwargs.get('workspace_id', None):
            if request.path == reverse('get_workspace', kwargs={'workspace_id': workspace_id}):
                workspace = Workspace.objects.get(id=workspace_id)
                if not workspace.does_user_have_access(request.user):
                    return JsonResponse({
                        "message": "You do not have permission to access this workspace!"
                    }, status=403)

                # Return workspace
                return JsonResponse({
                    "workspace": workspace.to_dict()
                }, status=200)

        # Get all workspaces
        # User can only view workspaces that they have access to
        if request.path == reverse('get_all_workspaces'):
            # Workspaces that user has access to
            workspaces = [
                workspace.to_dict()
                for workspace in Workspace.objects.all()
                if workspace.does_user_have_access(request.user)
            ]

            # Return workspaces
            return JsonResponse({
                "workspaces": workspaces
            }, status=200)

        # Return error message if path is invalid
        return JsonResponse({
            "message": "Invalid path!"
        }, status=400)


    # Disable unused argument **kwargs as it is required by Django
    # pylint: disable-next=unused-argument
    def post(self, request, **kwargs):
        """
        Create a new workspace
        """
        # User must be authenticated to create a workspace
        if request.user.is_anonymous:
            return JsonResponse({
                "message": "You must be authenticated to create a workspace!"
            }, status=401)

        # Create a new workspace
        data = json.loads(request.body)
        form = WorkspaceForm(data)
        if form.is_valid():
            workspace = form.save(commit=False)
            workspace.created_by = request.user
            workspace.save()
            workspace.members.add(request.user)

            # Return success message
            return JsonResponse({
                "message": f"Create workspace {form.cleaned_data['name']} successfully!",
                "workspace": workspace.to_dict()
            }, status=201)

        # Return error message if POST data is invalid
        return JsonResponse({
            "message": "Failed to create workspace!",
            'errors': form.errors
        }, status=400)

    def put(self, request, **kwargs):
        """
        Update a workspace
        """
        # User must be authenticated to update a workspace
        if request.user.is_anonymous:
            return JsonResponse({
                "message": "You must be authenticated to update a workspace!"
            }, status=401)

        # Must provide workspace_id as a param in path
        if 'workspace_id' not in kwargs:
            return JsonResponse({
                "message": "Missing workspace id"
            }, status=400)

        workspace_id = kwargs['workspace_id']

        # Update a workspace
        # Only the owner of the workspace can update it
        if request.path == reverse('update_workspace', kwargs={'workspace_id': workspace_id}):
            data = json.loads(request.body)
            workspace = Workspace.objects.get(id=workspace_id)
            if workspace.created_by != request.user:
                return JsonResponse({
                    "message": "You do not have permission to update this workspace!"
                }, status=403)

            form = WorkspaceForm(data, instance=workspace)
            if form.is_valid():
                workspace = form.save()
                workspace.save()

                # Return success message
                return JsonResponse({
                    "message": f"Update workspace {form.cleaned_data['name']} successfully!",
                    "workspace": workspace.to_dict()
                }, status=200)

            # Return error message if PUT data is invalid
            return JsonResponse({
                "message": "Failed to update workspace!",
                'errors': form.errors
            }, status=400)

        # Update sharing settings in a workspace
        # Only the owner of the workspace can update sharing settings
        if request.path == reverse(
            'update_sharing_settings',
            kwargs={'workspace_id': workspace_id}
        ):
            data = json.loads(request.body)
            workspace = Workspace.objects.get(id=workspace_id)
            if workspace.created_by != request.user:
                return JsonResponse({
                    "message": "You do not have permission to update this workspace's members!"
                }, status=403)

            sharing_option = data.get('sharing_option', "individuals")
            members = data.get('members', None)

            if sharing_option == "individuals" and isinstance(members, list):
                # Remove all members before adding new members to workspace
                workspace.members.clear()

                # Add members to workspace if found an user with the provided email
                for member in members:
                    if user := User.objects.filter(username=member).first():
                        workspace.members.add(user)

            workspace.sharing_option = sharing_option
            workspace.save()

            # Return success message
            return JsonResponse({
                "message": "Update workspace members successfully!",
                "workspace": workspace.to_dict()
            }, status=200)

        # Update position of doc lists in a workspace
        # Users with access to the workspace can update the position of doc lists
        if request.path == reverse(
            'update_doc_lists_position',
            kwargs={'workspace_id': workspace_id}
        ):
            data = json.loads(request.body)
            workspace = Workspace.objects.get(id=workspace_id)
            if not workspace.does_user_have_access(request.user):
                return JsonResponse({
                    "message": "You do not have permission to update this workspace's doc lists position!"
                }, status=403)

            doc_list_ids = data.get('docListIds', [])
            workspace.arrage_doc_lists(doc_list_ids)

            # Return success message
            return JsonResponse({
                "message": "Update doc lists position successfully!",
                "workspace": workspace.to_dict()
            }, status=200)

        # Return error message if path is invalid
        return JsonResponse({
            "message": "Invalid path!"
        }, status=400)

    def delete(self, request, **kwargs):
        """
        Delete a workspace
        """
        # User must be authenticated to delete a workspace
        if request.user.is_anonymous:
            return JsonResponse({
                "message": "You must be authenticated to delete a workspace!"
            }, status=401)

        # Must provide workspace_id as a param in path
        if 'workspace_id' not in kwargs:
            return JsonResponse({
                "message": "Missing workspace id"
            }, status=400)

        workspace_id = kwargs['workspace_id']

        # Delete a workspace
        # Only the owner of the workspace can delete it
        if request.path == reverse('delete_workspace', kwargs={'workspace_id': workspace_id}):
            workspace = Workspace.objects.get(id=workspace_id)
            if workspace.created_by != request.user:
                return JsonResponse({
                    "message": "You do not have permission to delete this workspace!"
                }, status=403)

            workspace.delete()

            # Return success message
            return JsonResponse({
                "message": "Delete workspace successfully!"
            }, status=200)

        # Return error message if path is invalid
        return JsonResponse({
            "message": "Invalid path!"
        }, status=400)


# Temporary disable CSRF protection for proof of concept
# @method_decorator(csrf_exempt, name='dispatch')
class DocListAPIView(View):
    """
    Handle API calls to manipulate doc lists
    """
    def get(self, request, **kwargs):
        """
        Get all doc lists in a workspace
        """
        # User must be authenticated to get doc lists
        if request.user.is_anonymous:
            return JsonResponse({
                "message": "You must be authenticated to get doc lists!"
            }, status=401)

        # Get all doc lists in a workspace
        # User with access to the workspace can view the doc lists
        if workspace_id := kwargs.get('workspace_id', None):
            if request.path == reverse('get_all_doc_lists_from_workspace', kwargs={'workspace_id': workspace_id}):
                workspace = Workspace.objects.get(id=workspace_id)
                if not workspace.does_user_have_access(request.user):
                    return JsonResponse({
                        "message": "You do not have permission to access this workspace!"
                    }, status=403)

                doc_lists = workspace.get_doc_lists()

                # Return doc lists
                return JsonResponse({
                    "doc_lists": doc_lists
                }, status=200)

        # Return error message if path is invalid
        return JsonResponse({
            "message": "Invalid path!"
        }, status=400)


    def post(self, request):
        """
        Create a new doc list
        """
        # User must be authenticated to create a doc list
        if request.user.is_anonymous:
            return JsonResponse({
                "message": "You must be authenticated to create a doc list!"
            }, status=401)

        # Create a new doc list
        # Users with access to the workspace can create a doc list
        data = json.loads(request.body)
        form = DocListForm(data)
        if form.is_valid():
            workspace = form.cleaned_data['workspace']
            if not workspace.does_user_have_access(request.user):
                return JsonResponse({
                    "message": "You do not have permission to create a doc list in this workspace!"
                }, status=403)

            doc_list = form.save()
            doc_list.save()

            # Return success message
            return JsonResponse({
                "message": f"Create doc list {form.cleaned_data['name']} successfully!",
                "doc_list": doc_list.to_dict()
            }, status=201)

        # Return error message if POST data is invalid
        return JsonResponse({
            "message": "Failed to create doc list!",
            'errors': form.errors
        }, status=400)

    def put(self, request, **kwargs):
        """
        Update a doc list
        """
        # User must be authenticated to update a doc list
        if request.user.is_anonymous:
            return JsonResponse({
                "message": "You must be authenticated to update a doc list!"
            }, status=401)

        # Must provide doc_list_id as a param in path
        if 'doc_list_id' not in kwargs:
            return JsonResponse({
                "message": "Missing doc list id"
            }, status=400)

        doc_list_id = kwargs['doc_list_id']

        # Update a doc list
        # Users with access to the doc list's workspace can update it
        if request.path == reverse('update_doc_lists', kwargs={'doc_list_id': doc_list_id}):
            data = json.loads(request.body)

            try:
                doc_list = DocList.objects.get(id=doc_list_id)
            except DocList.DoesNotExist:
                return JsonResponse({
                    "message": "Doc list not found!"
                }, status=404)

            if not doc_list.workspace.does_user_have_access(request.user):
                return JsonResponse({
                    "message": "You do not have permission to update this doc list!"
                }, status=403)

            form = DocListForm(data, instance=doc_list)
            if form.is_valid():
                doc_list = form.save()
                doc_list.save()

                # Return success message
                return JsonResponse({
                    "message": f"Update doc list {form.cleaned_data['name']} successfully!",
                    "doc_list": doc_list.to_dict()
                }, status=200)

            # Return error message if PUT data is invalid
            return JsonResponse({
                "message": "Failed to update doc list!",
                'errors': form.errors
            }, status=400)

        # Update position of a doc in a doc list
        # Users with access to the doc list's workspace can update the position of docs
        if request.path == reverse('update_docs_position', kwargs={'doc_list_id': doc_list_id}):
            data = json.loads(request.body)
            doc_list = DocList.objects.get(id=doc_list_id)
            if not doc_list.workspace.does_user_have_access(request.user):
                return JsonResponse({
                    "message": "You do not have permission to update this doc list's position!"
                }, status=403)

            doc_ids = data.get('docIds', [])
            doc_list.arrange_docs(doc_ids)

            # Return success message
            return JsonResponse({
                "message": "Update doc position successfully!",
                "doc_list": doc_list.to_dict()
            }, status=200)

        # Move a doc to another doc list
        # Users with access to the doc list's source and destination workspace can move docs
        if request.path == reverse('move_doc_to_list', kwargs={'doc_list_id': doc_list_id}):
            data = json.loads(request.body)
            doc_id = data.get('docId', None)
            if not doc_id:
                return JsonResponse({
                    "message": "Missing doc id!"
                }, status=400)

            doc = Doc.objects.get(id=doc_id)
            source_doc_list = doc.doc_list
            destination_doc_list = DocList.objects.get(id=doc_list_id)

            # User must have access to both the source and destination doc lists
            if ((not source_doc_list.workspace.does_user_have_access(request.user))
                or
                (not destination_doc_list.workspace.does_user_have_access(request.user))):
                return JsonResponse({
                    "message": "You do not have enough permission to move this doc!"
                }, status=403)

            doc.doc_list = destination_doc_list
            doc.save()

            source_doc_ids = data.get('sourceDocIds', [])
            destination_doc_ids = data.get('destinationDocIds', [])
            source_doc_list.arrange_docs(source_doc_ids)
            destination_doc_list.arrange_docs(destination_doc_ids)

            # Return success message
            return JsonResponse({
                "message": "Move doc successfully!",
                "doc": doc.to_dict()
            }, status=200)


        # Invalid path
        return JsonResponse({
            "message": "Invalid path!"
        }, status=400)

    def delete(self, request, **kwargs):
        """
        Delete a doc list
        """
        # User must be authenticated to delete a doc list
        if request.user.is_anonymous:
            return JsonResponse({
                "message": "You must be authenticated to delete a doc list!"
            }, status=401)

        # Must provide doc_list_id as a param in path
        if 'doc_list_id' not in kwargs:
            return JsonResponse({
                "message": "Missing doc list id"
            }, status=400)

        doc_list_id = kwargs['doc_list_id']

        # Delete a doc list
        # Users with access to the doc list's workspace can delete it
        if request.path == reverse('delete_doc_lists', kwargs={'doc_list_id': doc_list_id}):
            doc_list = DocList.objects.get(id=doc_list_id)
            if not doc_list.workspace.does_user_have_access(request.user):
                return JsonResponse({
                    "message": "You do not have permission to delete this doc list!"
                }, status=403)

            doc_list.delete()

            # Return success message
            return JsonResponse({
                "message": "Delete doc list successfully!"
            }, status=200)

        # Return error message if path is invalid
        return JsonResponse({
            "message": "Invalid path!"
        }, status=400)


# Temporary disable CSRF protection for proof of concept
# @method_decorator(csrf_exempt, name='dispatch')
class DocAPIView(View):
    """
    Handle API calls to manipulate docs
    """
    def get(self, request, **kwargs):
        """
        Get all docs in a doc list
        """
        # User must be authenticated to get docs
        if request.user.is_anonymous:
            return JsonResponse({
                "message": "You must be authenticated to get docs!"
            }, status=401)

        # Get all docs in a doc list
        # User with access to the doc list's workspace can view the docs
        if doc_list_id := kwargs.get('doc_list_id', None):
            if request.path == reverse(
                'get_all_docs_from_doc_list',
                kwargs={'doc_list_id': doc_list_id}):
                doc_list = DocList.objects.get(id=doc_list_id)
                if not doc_list.workspace.does_user_have_access(request.user):
                    return JsonResponse({
                        "message": "You do not have permission to access this list!"
                    }, status=403)

                docs = doc_list.doc_set.all()
                docs = [doc.to_dict() for doc in docs]

                # Return docs
                return JsonResponse({
                    "docs": docs
                }, status=200)

        # Return error message if path is invalid
        return JsonResponse({
            "message": "Invalid path!"
        }, status=400)

    # Disable unused argument **kwargs as it is required by Django
    # pylint: disable-next=unused-argument
    def post(self, request, **kwargs):
        """
        Create a new doc
        """
        # User must be authenticated to create a doc
        if request.user.is_anonymous:
            return JsonResponse({
                "message": "You must be authenticated to create a doc!"
            }, status=401)

        # Create a new doc
        # User with access to the doc list's workspace can create a doc
        if request.path == reverse('create_docs'):
            data = json.loads(request.body)
            form = DocForm(data)
            if form.is_valid():
                # Owner of the doc list must be the same as the authenticated user
                doc_list = form.cleaned_data['doc_list']
                if not doc_list.workspace.does_user_have_access(request.user):
                    return JsonResponse({
                        "message": "You do not have permission to create a doc in this list!"
                    }, status=403)

                doc = form.save()
                doc.created_by = request.user
                doc.contributors.add(request.user)
                doc.save()

                # Create a main version for this doc with empty content
                doc.version_set.create(
                    version='main',
                    content='',
                    is_main=True
                )

                # Return success message
                return JsonResponse({
                    "message": f"Create doc {form.cleaned_data['name']} successfully!",
                    "doc": doc.to_dict()
                }, status=201)

        # Search for docs
        # User provide a search query in the request body
        # Return docs that are in workspaces that the user has access to
        if request.path == reverse('search_docs'):
            data = json.loads(request.body)
            query = data.get('query', '')

            docs = Doc.objects.filter(
                Q(name__icontains=query) |
                Q(tags__icontains=query) |
                Q(version__content__icontains=query)  # Search in Version model's content field
            ).distinct()

            # Filter out docs that the user does not have access to
            docs = [
                doc.to_dict()
                for doc in docs
                if doc.doc_list.workspace.does_user_have_access(request.user)]

            # Return search results
            return JsonResponse({
                "docs": docs
            }, status=200)

        # Return error message if POST data is invalid
        return JsonResponse({
            "message": "Failed to create doc!",
            'errors': form.errors
        }, status=400)

    def put(self, request, **kwargs):
        """
        Update a doc
        """
        # User must be authenticated to update a doc
        if request.user.is_anonymous:
            return JsonResponse({
                "message": "You must be authenticated to update a doc!"
            }, status=401)

        # Must provide doc_id as a param in path
        if 'doc_id' not in kwargs:
            return JsonResponse({
                "message": "Missing doc id"
            }, status=400)

        doc_id = kwargs['doc_id']

        # Update a doc
        # User with access to the doc's workspace can update it
        if request.path == reverse('update_docs', kwargs={'doc_id': doc_id}):
            data = json.loads(request.body)
            doc = Doc.objects.get(id=doc_id)
            if not doc.doc_list.workspace.does_user_have_access(request.user):
                return JsonResponse({
                    "message": "You do not have permission to update this doc!"
                }, status=403)

            form = DocForm(data, instance=doc)
            if form.is_valid():
                doc = form.save(commit=False)
                doc.contributors.add(request.user)
                doc.save()

                # Update versions of the doc
                if versions := data.get('versions', None):
                    for version in versions:
                        version_id = version.get('id', None)
                        # Check if the version belongs to the doc
                        if not doc.version_set.filter(id=version_id).exists():
                            return JsonResponse({
                                "message": "Version not found in the doc!"
                            }, status=404)

                        updated_version = Version.objects.get(id=version_id)
                        updated_version.version = version.get('version', f"Version {version_id}")
                        updated_version.content = version.get('content', "")
                        updated_version.save()

                # Delete all deleted versions
                if deleted_versions := data.get('deleted_versions', None):
                    for version_id in deleted_versions:
                        if version := doc.version_set.filter(id=version_id).first():
                            version.delete()

                # Return success message
                return JsonResponse({
                    "message": f"Update doc {form.cleaned_data['name']} successfully!",
                    "doc": doc.to_dict(),
                    "versions": [version.to_dict() for version in doc.version_set.all()]
                }, status=200)

            # Return error message if PUT data is invalid
            return JsonResponse({
                "message": "Failed to update doc!",
                'errors': form.errors
            }, status=400)

        # Move a doc to a different workspace
        # Users with access to the doc's workspace can move docs
        # to a different workspace where all contributors of the doc have access to
        if request.path == reverse('move_doc_to_workspace', kwargs={'doc_id': doc_id}):
            data = json.loads(request.body)
            new_doc_name = data.get('new_doc_name', None)
            workspace_id = data.get('new_workspace_id', None)
            new_doc_list_id = data.get('new_doc_list_id', None)
            if not workspace_id or not new_doc_list_id:
                return JsonResponse({
                    "message": "Missing workspace id or doc list id!"
                }, status=400)

            # Check if doc exists
            try:
                doc = Doc.objects.get(id=doc_id)
            except Doc.DoesNotExist:
                return JsonResponse({
                    "message": "Doc not found!"
                }, status=404)

            # User must be the author or a contributor of the doc
            if request.user != doc.created_by and request.user not in doc.contributors.all():
                return JsonResponse({
                    "message": "You do not have permission to move this doc!"
                }, status=403)

            # Check if the new workspace exists
            try:
                new_workspace = Workspace.objects.get(id=workspace_id)
            except Workspace.DoesNotExist:
                return JsonResponse({
                    "message": "Workspace not found!"
                }, status=404)

            # Check if the new doc list exists
            try:
                new_doc_list = DocList.objects.get(id=new_doc_list_id)
            except DocList.DoesNotExist:
                return JsonResponse({
                    "message": "Doc list not found!"
                }, status=404)

            # User must have access to the new workspace
            if not new_workspace.does_user_have_access(request.user):
                return JsonResponse({
                    "message": "You do not have permission to move this doc!"
                }, status=403)

            # All contributors of the doc must have access to the new workspace
            if not all(new_workspace.does_user_have_access(contributor) for contributor in doc.contributors.all()):
                return JsonResponse({
                    "message": "The new workspace must have all of the contributors of the doc!"
                }, status=400)

            # Move the doc to the new doc list
            if new_doc_name:
                doc.name = new_doc_name
            old_workspace = doc.doc_list.workspace
            doc.doc_list = new_doc_list
            doc.save()

            # Return success message
            return JsonResponse({
                "message": "Move doc successfully!",
                # If move the doc within the same workspace
                # Return the resulting doc lists of that workspace
                "doc_lists": old_workspace.get_doc_lists()
            }, status=200)

        # Invalid path
        return JsonResponse({
            "message": "Invalid path!"
        }, status=400)

    def delete(self, request, **kwargs):
        """
        Delete a doc
        """
        # User must be authenticated to delete a doc
        if request.user.is_anonymous:
            return JsonResponse({
                "message": "You must be authenticated to delete a doc!"
            }, status=401)

        # Must provide doc_id as a param in path
        if 'doc_id' not in kwargs:
            return JsonResponse({
                "message": "Missing doc id"
            }, status=400)

        doc_id = kwargs['doc_id']

        # Delete a doc
        # User with access to the doc's workspace can delete the doc
        # and user must be the author or a contributor of the doc
        if request.path == reverse('delete_docs', kwargs={'doc_id': doc_id}):
            doc = Doc.objects.get(id=doc_id)
            if (not doc.doc_list.workspace.does_user_have_access(request.user)
                or (request.user != doc.created_by and request.user not in doc.contributors.all())):
                return JsonResponse({
                    "message": "You do not have permission to delete this doc!"
                }, status=403)

            doc.delete()

            # Return success message
            return JsonResponse({
                "message": "Delete doc successfully!"
            }, status=200)

        # Return error message if path is invalid
        return JsonResponse({
            "message": "Invalid path!"
        }, status=400)


# Temporary disable CSRF protection for proof of concept
# @method_decorator(csrf_exempt, name='dispatch')
class VersionAPIView(View):
    """
    Handle API calls to manipulate versions
    """
    def get(self, request, **kwargs):
        """
        Get all versions of a doc
        """
        # User must be authenticated to get versions
        if request.user.is_anonymous:
            return JsonResponse({
                "message": "You must be authenticated to get versions!"
            }, status=401)

        # Get all versions of a doc
        # User with access to the doc's workspace can view the versions
        if doc_id := kwargs.get('doc_id', None):
            if request.path == reverse('get_all_versions_from_doc', kwargs={'doc_id': doc_id}):
                doc = Doc.objects.get(id=doc_id)
                if not doc.doc_list.workspace.does_user_have_access(request.user):
                    return JsonResponse({
                        "message": "You do not have permission to access this doc!"
                    }, status=403)

                versions = doc.version_set.all()
                versions = [version.to_dict() for version in versions]

                # Return versions
                return JsonResponse({
                    "versions": versions
                }, status=200)

        # Return error message if path is invalid
        return JsonResponse({
            "message": "Invalid path!"
        }, status=400)

    # Disable unused argument **kwargs as it is required by Django
    # pylint: disable-next=unused-argument
    def post(self, request, **kwargs):
        """
        Create a new version
        """
        # User must be authenticated to create a version
        if request.user.is_anonymous:
            return JsonResponse({
                "message": "You must be authenticated to create a version!"
            }, status=401)

        # Create a new version
        # User with access to the doc's workspace can create a version
        if request.path == reverse('create_versions'):
            data = json.loads(request.body)
            doc_id = data.get('doc', None)
            version = data.get('version', None)
            is_main = data.get('is_main', False)
            content = data.get('content', None)
            if not doc_id or not version:
                return JsonResponse({
                    "message": "Missing doc id or version number!"
                }, status=400)

            # Check if doc exists
            try:
                doc = Doc.objects.get(id=doc_id)
            except Doc.DoesNotExist:
                return JsonResponse({
                    "message": "Doc not found!"
                }, status=404)

            # User with access to the doc's workspace can create a version
            if not doc.doc_list.workspace.does_user_have_access(request.user):
                return JsonResponse({
                    "message": "You do not have permission to create a version for this doc!"
                }, status=403)

            # Create a new version
            version = Version(doc=doc, version=version, is_main=is_main, content=content)
            version.save()

            # Return success message
            return JsonResponse({
                "message": "Create version successfully!",
                "version": version.to_dict()
            }, status=201)

        # Return error message if POST data is invalid
        return JsonResponse({
            "message": "Failed to create version!"
        }, status=400)
