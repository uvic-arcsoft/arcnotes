# pylint: disable=W0611
# W0611 disabled this import is default it sets the page up
from django.contrib import admin
from .models import Workspace, DocList, Doc, Version

# Register your models here.
admin.site.register(Workspace)
admin.site.register(DocList)
admin.site.register(Doc)
admin.site.register(Version)
