# pylint:
from django.urls import path
from . import views

urlpatterns = [
    # Workspaces
    path('workspaces/create', views.WorkspaceAPIView.as_view(), name='create_workspaces'),
    path('workspaces/get_all', views.WorkspaceAPIView.as_view(), name='get_all_workspaces'),
    path(
        'workspaces/get/<int:workspace_id>',
        views.WorkspaceAPIView.as_view(),
        name='get_workspace'),
    path(
        'workspaces/update/<int:workspace_id>',
        views.WorkspaceAPIView.as_view(),
        name='update_workspace'),
    path('workspaces/update_sharing_settings/<int:workspace_id>',
        views.WorkspaceAPIView.as_view(), name='update_sharing_settings'),
    path('workspaces/delete/<int:workspace_id>',
        views.WorkspaceAPIView.as_view(), name='delete_workspace'),
    path('workspaces/update_doc_lists_position/<int:workspace_id>',
        views.WorkspaceAPIView.as_view(), name='update_doc_lists_position'),

    # Doc lists
    path('doc_lists/create', views.DocListAPIView.as_view(), name='create_doc_lists'),
    path(
        'doc_lists/update/<int:doc_list_id>',
        views.DocListAPIView.as_view(),
        name='update_doc_lists'),
    path('doc_lists/get_all_from_workspace/<int:workspace_id>',
        views.DocListAPIView.as_view(), name='get_all_doc_lists_from_workspace'),
    path(
        'doc_lists/delete/<int:doc_list_id>',
        views.DocListAPIView.as_view(),
        name='delete_doc_lists'),
    path('doc_lists/update_docs_position/<int:doc_list_id>',
        views.DocListAPIView.as_view(), name='update_docs_position'),
    path(
        'doc_lists/move_doc_to_list/<int:doc_list_id>',
        views.DocListAPIView.as_view(),
        name='move_doc_to_list'),

    # Docs
    path('docs/create', views.DocAPIView.as_view(), name='create_docs'),
    path(
        'docs/get_all_from_doc_list/<int:doc_list_id>',
        views.DocAPIView.as_view(),
        name='get_all_docs_from_doc_list'),
    path('docs/update/<int:doc_id>', views.DocAPIView.as_view(), name='update_docs'),
    path('docs/delete/<int:doc_id>', views.DocAPIView.as_view(), name='delete_docs'),
    path('docs/search', views.DocAPIView.as_view(), name='search_docs'),
    path('docs/move/<int:doc_id>', views.DocAPIView.as_view(), name='move_doc_to_workspace'),

    # Versions,
    path(
        'versions/get_all_from_doc/<int:doc_id>',
        views.VersionAPIView.as_view(),
        name='get_all_versions_from_doc'),
    path(
        'versions/create',
        views.VersionAPIView.as_view(),
        name='create_versions'),
]
