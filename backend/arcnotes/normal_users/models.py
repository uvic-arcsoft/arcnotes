# pylint:
from django.db import models

# Workspaces:
# A workspace has multiple docs and can be shared with multiple users
class Workspace(models.Model):
    name = models.CharField(max_length=100)
    created_by = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    members = models.ManyToManyField('auth.User', related_name='workspaces', blank=True)
    sharing_option = models.CharField(max_length=20, default='individuals', choices={
        "private": "private",
        "authenticated": "authenticated",
        "individuals": "individuals",
        "public": "public"
    }, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'workspaces'
        ordering = ['created_at']

    def __str__(self):
        return str(self.name)

    def to_dict(self):
        """Return a dictionary representation of the workspace

        Returns:
            dict: A dictionary representation of the workspace
        """
        return {
            "id": self.id,
            "name": self.name,
            "created_by": {
                "id": self.created_by.id,
                "username": self.created_by.username,
                # Add any other User model fields you need
            },
            "members": [{
                "id": member.id,
                "username": member.username
            } for member in self.members.all()],
            "sharing_option": self.sharing_option,
            "tags": self.get_tags(),
            "doc_lists": [{
                "id": doc_list.id,
                "name": doc_list.name,
                "created_at": doc_list.created_at.isoformat(),  # Convert datetime to string
            } for doc_list in self.doclist_set.all().order_by('position')],
            "created_at": self.created_at.isoformat(),  # Convert datetime to string
        }

    def get_doc_lists(self):
        """Return a list of all doc lists in the workspace

        Returns:
            list: A list of all doc lists in the workspace
        """
        return [doc_list.to_dict() for doc_list in self.doclist_set.all().order_by('position')]

    def get_tags(self):
        """Return a list of all tags in the workspace

        Returns:
            list: A list of all tags in the workspace
        """
        tags = set()
        for doc_list in self.doclist_set.all():
            for doc in doc_list.doc_set.all():
                if doc.tags:
                    tags.update([tag.strip() for tag in doc.tags.split(",") if tag])
        return list(tags)

    def arrage_doc_lists(self, doc_list_ids):
        """Arrange the doc lists in the workspace based on the order of doc_list_ids

        Args:
            doc_list_ids (list): A list of doc list ids in the desired order
        """
        for index, doc_list_id in enumerate(doc_list_ids):
            doc_list = self.doclist_set.get(id=doc_list_id)
            if doc_list.position != index + 1:
                doc_list.position = index + 1
                doc_list.save()

    def does_user_have_access(self, user):
        """Check if the user has access to the workspace

        Args:
            user (auth.User): The user object
        Returns:
            bool: True if the user has access to the workspace, False otherwise
        """
        return (
            self.sharing_option in ["public", "authenticated"]
            or self.created_by == user
            or (user in self.members.all() and self.sharing_option == "individuals")
        )


# Documentation list:
# A list contains multiple docs
# A workspace can have multiple lists
class DocList(models.Model):
    name = models.CharField(max_length=100)
    workspace = models.ForeignKey(Workspace, on_delete=models.CASCADE)
    position = models.PositiveIntegerField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'doc_lists'
        ordering = ['created_at', 'position']

    def save(self, *args, **kwargs):
        if self.position is None:
            last_item = self.workspace.doclist_set.all().order_by('position').last()
            self.position = (last_item.position + 1) if last_item and last_item.position else 1
        super().save(*args, **kwargs)

    def __str__(self):
        return str(self.name)

    def to_dict(self):
        """Return a dictionary representation of the doc list

        Returns:
            dict: A dictionary representation of the doc list
        """
        return {
            "id": self.id,
            "name": self.name,
            "workspace": self.workspace.id,
            "docs": [doc.to_dict() for doc in self.doc_set.all().order_by('position')],
            "created_at": self.created_at.isoformat(),  # Convert datetime to string
        }

    def arrange_docs(self, doc_ids):
        """Arrange the docs in the list based on the order of doc_ids

        Args:
            doc_ids (list): A list of doc ids in the desired order
        """
        for index, doc_id in enumerate(doc_ids):
            doc = self.doc_set.get(id=doc_id)
            if doc.position != index + 1:
                doc.position = index + 1
                doc.save()


# A documentation:
# A doc belongs to a list
class Doc(models.Model):
    name = models.CharField(max_length=100)
    doc_list = models.ForeignKey(DocList, on_delete=models.CASCADE)
    tags = models.CharField(max_length=200, blank=True, null=True)
    position = models.PositiveIntegerField(blank=True, null=True)
    created_by = models.ForeignKey('auth.User', on_delete=models.SET_NULL, null=True, blank=True)
    contributors = models.ManyToManyField('auth.User', related_name='contributed_docs', blank=True)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'docs'
        ordering = ['created_at', 'position']

    def save(self, *args, **kwargs):
        # Set the position to the last item if not explicitly set
        if self.position is None:
            last_item = self.doc_list.doc_set.all().order_by('position').last()
            self.position = (last_item.position + 1) if last_item and last_item.position else 1
        super().save(*args, **kwargs)

    def __str__(self):
        return str(self.name)

    def to_dict(self):
        """Return a dictionary representation of the doc

        Returns:
            dict: A dictionary representation of the doc
        """
        return {
            "id": self.id,
            "name": self.name,
            "doc_list": self.doc_list.id,
            "workspace": self.doc_list.workspace.id,
            "tags": [tag.strip() for tag in self.tags.split(",") if tag] if self.tags else [],
            "position": self.position,
            "created_by": self.created_by.username if self.created_by else None,
            "contributors": [contributor.username for contributor in self.contributors.all()],
            "created_at": self.created_at.isoformat(),  # Convert datetime to string
        }

    def does_user_have_access(self, user):
        """Check if the user has access to the doc
        Params: auth.User object
        Returns:
            bool: True if the user has access to the doc, False otherwise
        """
        return self.doc_list.workspace.does_user_have_access(user)

# A version for documentation's content
# A doc content can have multiple versions
class Version(models.Model):
    version = models.CharField(max_length=100)
    content = models.TextField(blank=True, default="")
    is_main = models.BooleanField(default=False)
    doc = models.ForeignKey(Doc, on_delete=models.CASCADE)
    last_updated = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'doc_versions'
        ordering = ['id']

    def __str__(self):
        return str(self.doc.name) + " - " + str(self.version)

    def to_dict(self):
        """Return a dictionary representation of the version

        Returns:
            dict: A dictionary representation of the version
        """
        return {
            "id": self.id,
            "version": self.version,
            "content": self.content,
            "is_main": self.is_main,
            "doc": self.doc.id,
            "last_updated": self.last_updated.isoformat(),  # Convert datetime to string
        }
