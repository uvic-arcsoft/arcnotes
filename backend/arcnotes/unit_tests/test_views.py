# pylint:
from django.test import TestCase, Client
from django.contrib.auth.models import User

class ViewTestCase(TestCase):
    """Views test case class"""
    def setUp(self):
        # Create an user with admin rights in the testing database
        self.fake_admin = User.objects.create(username="admin@example.org", password="testingadmin", is_staff=True)

        # Fake client with admin rights
        self.client = Client(HTTP_X_FORWARDED_USER="admin@example.org")
        s = self.client.session
        s.update({"user": self.fake_admin.id, "is_admin": True})
        s.save()

    def test_get_normal_user_dashboard(self):
        """Test getting normal user dashboard"""
        response = self.client.get("/")
        self.assertEqual(response.status_code, 200)
