# Use the official Nginx image as a parent image
FROM nginx:1.27.0-alpine

# Remove any existing configuration files
RUN rm /etc/nginx/conf.d/default.conf

# Copy the Nginx configuration file into the container
COPY deployments/nginx/nginx.conf /etc/nginx/nginx.conf

# Copy Django collected static files
RUN mkdir -p /usr/src/app/static
COPY arcnotes/staticfiles /usr/src/app/static/

# Set the user 'nginx' to have a specific UID
RUN set -x \
    && apk --no-cache add shadow \
    && usermod -u 1000 nginx \
    && groupmod -g 1000 nginx

# Create necessary directories and set permissions
RUN mkdir -p /var/cache/nginx/client_temp /var/cache/nginx/proxy_temp /var/cache/nginx/fastcgi_temp /var/cache/nginx/uwsgi_temp /var/cache/nginx/scgi_temp \
    && mkdir -p /var/cache/nginx/pid \
    && chown -R nginx:nginx /var/cache/nginx /var/log/nginx /usr/share/nginx/html /etc/nginx \
    && chmod -R 755 /var/cache/nginx /usr/share/nginx/html /etc/nginx

# Set the user to run the Nginx process, don't run as root
USER nginx

# Expose ports for HTTP and HTTPS
EXPOSE 9001

# Start Nginx when the container launches
CMD ["nginx", "-g", "daemon off;"]
