# pylint:
import sys
import subprocess
import os

# Get the current version set in settings.py
version = subprocess.check_output([
    'python3',
    'arcnotes/manage.py',
    'shell',
    '-c',
    "from django.conf import settings; print(settings.CONFIG['PROJECT_VERSION'])"]).decode('UTF-8').rstrip()
version_description = subprocess.check_output([
    'python3',
    'arcnotes/manage.py',
    'shell',
    '-c',
    "from django.conf import settings; print(settings.CONFIG['VERSION_DESCRIPTION'])"]).decode('UTF-8').rstrip()
print('Selected version: ' + version)
print('Version description: ' + version_description)

# Get the current git tags, if the tag specified in
# CONFIG['PROJECT_VERSION'] already exists, this script should fail and stop being executed
tags = subprocess.check_output(['git', 'tag']).decode('UTF-8').split('\n')
if version in tags:
    print('This version already exists, please select a different one')
    sys.exit()

# Run git tag with tag as version and annotated message as the version description
os.system(f"git tag -a {version} -m '{version_description}'")
print('Current tags:')
os.system('git tag')

# Write the new version to arcnotes/version.py
# TODO: Fix consider-using-with later
# pylint: disable-next=consider-using-with
version_file = open('arcnotes/version.py', 'w', encoding='utf-8')
version_file.write('# pylint:\n')
version_file.write('# This file keep tracks of the current app version\n')
version_file.write(f"verion = '{version}'\n")
version_file.write(f"verion_description = '{version_description}'\n")
version_file.close()

# git add and commit arcnotes/version.py
os.system('git add arcnotes/version.py')
os.system("git commit -m 'Update version for the application'")
