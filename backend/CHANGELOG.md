### v0.1 - Delete this script as it is now a rule in Makefile
### v0.1.1 - Add automation scripts for Makefile
### v0.1.2 - Automate creating users using Makefile
### v0.1.2-1 - Fix createusers.py script error
### v0.1.3 - Add a documentation for using Makefile
### v0.1.4 - Render username on navigation bar
### v0.1.5 - Remove Windows stuffs, configure local testings and fix update changelog script
